package deployemetSimple;


import Component.Broker;
import Component.Publisher;
import Component.Subscriber;

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.cvm.AbstractCVM;

/**
 * 
 * @author Derbene,sh ahmadi simab
 * 
 *
 */


public class CVM extends AbstractCVM{
	
	public  static String URI_PUBLICATION = "URI_PUBLICATION";
	public   static String URI_MANAGEMENT= "URI_MANAGEMENT";
	public   static String URI_BROKER_SUBSCRIBER_RECEPTION = "URI_BROKER_SUBSCRIBER_RECEPTION";
	public   static String URI_SUBSCRIBER_BROKER_RECEPTION = "URI_SUBSCRIBER_BROKER_RECEPTION";
	
	public final static String	PUBLISHER_COMPONENT_URI = "producteur-uri" ;
	public final static String	SUBSCRIBER_COMPONENT_URI = "consommateur-uri" ;
	
	
	public CVM() throws Exception {
		super(false);

	}

	@Override
	public void deploy() throws Exception {
		// TODO Auto-generated method stub
		String uriBroker=
				AbstractComponent.createComponent(
						Broker.class.getCanonicalName(), 
						new Object[]{URI_PUBLICATION,URI_MANAGEMENT,URI_BROKER_SUBSCRIBER_RECEPTION,URI_SUBSCRIBER_BROKER_RECEPTION}
				);
		this.toggleTracing(uriBroker);
		String uriPublisher=
				AbstractComponent.createComponent(
						Publisher.class.getCanonicalName(), 
						new Object[]{URI_PUBLICATION,URI_MANAGEMENT}
				);
		this.toggleTracing(uriPublisher);
		
//		String uriPublisherPlugin=
//				AbstractComponent.createComponent(
//						fr.sorbonne_upmc.plugins.component.Publisher.class.getCanonicalName(), 
//						new Object[]{URI_BROKER_PUBLISHER_PUBLICATION,URI__BROKER_PUBLISHER_MANAGEMENT}
//				);
//		this.toggleTracing(uriPublisherPlugin);
//		
		String uriSubscriber=
				AbstractComponent.createComponent(
						Subscriber.class.getCanonicalName(), 
						new Object[]{URI_SUBSCRIBER_BROKER_RECEPTION,URI_MANAGEMENT}
				);
		this.toggleTracing(uriSubscriber);
		
		super.deploy();
	
	}
	public static void main(String[] args) {
		try {
			CVM cvm = new CVM() ;
			cvm.startStandardLifeCycle(60000L) ;
			Thread.sleep(5000L) ;
			System.exit(0) ;
		} catch (Exception e) {
			throw new RuntimeException(e) ;
		}		

	}
}
