package PublisherSubscriber.Port;

import java.util.ArrayList;

import Component.Broker;
import Message.MessageI;
import PublisherSubscriber.interfaces.ManagementCI;
import PublisherSubscriber.interfaces.PublicationCI;
import PublisherSubscriber.interfaces.ManagementImplementation;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.AbstractPlugin;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;
import fr.sorbonne_u.components.ports.forplugins.AbstractInboundPortForPlugin;

public class PublicationInboundPort  extends AbstractInboundPort implements PublicationCI {
	
	private static final long serialVersionUID = 1L;
	public PublicationInboundPort( ComponentI owner) throws Exception {
		super( PublicationCI.class, owner) ;

		assert owner instanceof Broker;
		
	}
	
	public PublicationInboundPort(String uri,ComponentI owner) throws Exception {
		super(uri, PublicationCI.class,  owner);
		assert owner instanceof ManagementImplementation;
	
	}



	@Override
	public void publish(MessageI m, String topic) throws Exception {
		this.owner.handleRequestAsync(
				new AbstractComponent.AbstractService<Void>() {
					@Override
					public Void call() throws Exception {
						((Component.Broker)this.getServiceOwner()).publish(m, topic);
						return null;
					}
				}) ;
		
		
	}

	@Override
	public void publish(MessageI m, String[] topic) throws Exception{
		this.owner.handleRequestAsync(
				new AbstractComponent.AbstractService<Void>() {
					@Override
					public Void call() throws Exception {
						((Component.Broker)this.getServiceOwner()).publish(m, topic);
						return null;
					}
				}) ;
		
	}
	@Override
	public void publish(MessageI[] m, String[] topic) throws Exception{
		this.owner.handleRequestAsync(
				new AbstractComponent.AbstractService<Void>() {
					@Override
					public Void call() throws Exception {
						((Component.Broker)this.getServiceOwner()).publish(m, topic);
						return null;
					}
				}) ;
		
	}

	@Override
	public void publish(MessageI[] m, String topic)throws Exception {
		this.owner.handleRequestAsync(
				new AbstractComponent.AbstractService<Void>() {
					@Override
					public Void call() throws Exception {
						((Component.Broker)this.getServiceOwner()).publish(m, topic);
						return null;
					}
				}) ;
		
	}


}
