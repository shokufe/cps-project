package PublisherSubscriber.Port;



import java.util.ArrayList;

import Message.MessageI;
import PublisherSubscriber.interfaces.PublicationCI;
import fr.sorbonne_u.components.ComponentI;

import fr.sorbonne_u.components.ports.AbstractOutboundPort;

public class PublicationOutboundPort extends AbstractOutboundPort implements PublicationCI {
	
	private static final long serialVersionUID = 1L;

	public PublicationOutboundPort(String uri, ComponentI owner) throws Exception {
		super(uri,PublicationCI.class, owner);
		//System.out.println("constructor");

		assert	uri != null && owner != null ;	
	}
	
	public PublicationOutboundPort( ComponentI owner) throws Exception {
		super(PublicationCI.class, owner);
	
	}



	@Override
	public void publish(MessageI m, String topic) throws Exception {
		System.out.println("publish out");
		((PublicationCI)this.connector).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI m,String[] topic) throws Exception{
		((PublicationCI)this.connector).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI[] m, String[] topic) throws Exception{
		((PublicationCI)this.connector).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI[] m, String topic) throws Exception{
		((PublicationCI)this.connector).publish(m, topic);
		
	}
	




}
