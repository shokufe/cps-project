package PublisherSubscriber.Port;


import java.util.ArrayList;

import Component.Broker;
import Message.MessageFilterI;
import PublisherSubscriber.interfaces.ManagementCI;
import PublisherSubscriber.interfaces.ManagementImplementation;

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;
import fr.sorbonne_u.components.ports.forplugins.AbstractInboundPortForPlugin;

/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */

public class ManagementInboundPort extends AbstractInboundPort implements ManagementCI {


	private static final long serialVersionUID = 1L; 

	public ManagementInboundPort(ComponentI owner) throws Exception {
		super( ManagementCI.class, owner) ;
		assert owner instanceof ManagementImplementation;
	
	}

	
	public ManagementInboundPort(String uri,ComponentI owner) throws Exception {
		super(uri, ManagementCI.class,  owner);
		assert owner instanceof ManagementImplementation;
	
	}
	
	@Override
	public void subscribe(String topic, String inboundPortURI) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).subscribe(topic, inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void subscribe(String [] topic, String inboundPortURI) throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).subscribe(topic, inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void subscribe(String topic, MessageFilterI filter, String inboundPortURI) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).subscribe(topic, filter, inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void modifyFilter(String topic, MessageFilterI newFilter, String inboundPortURI)throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).modifyFilter(topic, newFilter, inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void unsubscribe(String topic, String inboundPortURI)throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).unsubscribe(topic, inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void createTopic(String topic) throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).createTopic(topic);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void createTopics(String[] topics)throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).createTopics(topics);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void destroyTopic(String topic) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).destroyTopic(topic);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean isTopic(String topic) throws Exception{
		System.out.println("istopic");
		return this.getOwner().handleRequestSync(
				owner -> ((Broker)owner).isTopic(topic));

	}

	@Override
	public String [] getTopics() throws Exception{
		return this.getOwner().handleRequestSync(
				new AbstractComponent.AbstractService< String []>() {
					@Override
					public String[] call() throws Exception {
						return ((Broker)this.getServiceOwner()).getTopics() ;
					}
				}) ;
	}

	@Override
	public String getPublicationPortURI() throws Exception{
		return this.getOwner().handleRequestSync(
				new AbstractComponent.AbstractService<String>() {
					@Override
					public String call() throws Exception {
						return ((Broker)this.getServiceOwner()).getPublicationPortURI() ;
					}
				}) ;
	}

	
	
}
