package PublisherSubscriber.Port;

import java.util.ArrayList;

import Component.Subscriber;
import Message.MessageI;

import PublisherSubscriber.interfaces.ReceptionCI;
import PublisherSubscriber.interfaces.ReceptionImplementationI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;
import fr.sorbonne_u.components.ports.forplugins.AbstractInboundPortForPlugin;

public class ReceptionIntboundPort extends AbstractInboundPort implements ReceptionCI  {
	

	private static final long serialVersionUID = 1L;
	
	public ReceptionIntboundPort(ComponentI owner) throws Exception {
		super( ReceptionCI.class, owner) ;
		assert owner instanceof ReceptionImplementationI;
	
	}
	

	public ReceptionIntboundPort(String uri,ComponentI owner) throws Exception {
		super(uri, ReceptionCI.class, owner);
		assert owner instanceof ReceptionImplementationI;
	
	}


	@Override
	public void acceptMessage(MessageI m) throws Exception{
		
		  this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Subscriber)this.getServiceOwner()).acceptMessage(m) ;
							return null;
						}
					}) ;
	

		
	}

	@Override
	public void acceptMessages(MessageI[] ms) throws Exception{
		this.owner.handleRequestAsync(
				new AbstractComponent.AbstractService<Void>() {
					@Override
					public Void call() throws Exception {
						((Subscriber)this.getServiceOwner()).acceptMessages(ms) ;
						return null;
					}
				}) ;
	
	}

}
