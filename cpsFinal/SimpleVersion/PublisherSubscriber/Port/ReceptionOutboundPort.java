package PublisherSubscriber.Port;

import java.util.ArrayList;

import Component.Broker;
import Message.MessageI;
import PublisherSubscriber.interfaces.PublicationCI;
import PublisherSubscriber.interfaces.ReceptionCI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;

public class ReceptionOutboundPort extends AbstractOutboundPort implements ReceptionCI {

	private static final long serialVersionUID = 1L;
	
  
	public ReceptionOutboundPort(String uri,ComponentI owner) throws Exception {
		super(uri,ReceptionCI.class, owner);
		assert owner instanceof Broker;
	}
	
	
	public ReceptionOutboundPort(ComponentI owner) throws Exception
	{
		super(PublicationCI.class, owner);
		
	}
	
	
	
	@Override
	public void acceptMessage(MessageI m) throws Exception{
		((ReceptionCI)this.connector).acceptMessage(m);
		
	}

	@Override
	public void acceptMessages(MessageI[] m) throws Exception{
		((ReceptionCI)this.connector).acceptMessages(m);
		
	}

}
