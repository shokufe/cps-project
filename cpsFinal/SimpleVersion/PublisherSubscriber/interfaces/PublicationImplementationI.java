package PublisherSubscriber.interfaces;

import java.util.ArrayList;

import Message.MessageI;

public interface PublicationImplementationI {
	
	public void publish(MessageI m,String topic) throws Exception;
	public void publish(MessageI m,String[]topic) throws Exception;
	public void publish(MessageI[] m,String[]topic) throws Exception;
	public void publish(MessageI[] m,String topic) throws Exception;


	

}
