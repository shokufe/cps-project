package PublisherSubscriber.interfaces;

import fr.sorbonne_u.components.interfaces.OfferedI;
import fr.sorbonne_u.components.interfaces.RequiredI;

public interface ManagementLaunchI extends OfferedI,RequiredI {
	public void	getURIandPrint() throws Exception ;

}
