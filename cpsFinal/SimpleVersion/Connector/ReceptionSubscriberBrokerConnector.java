package Connector;

import java.util.ArrayList;

import Message.MessageI;
import PublisherSubscriber.interfaces.ReceptionCI;
import fr.sorbonne_u.components.connectors.AbstractConnector;

public class ReceptionSubscriberBrokerConnector extends AbstractConnector implements ReceptionCI  {

	@Override
	public void acceptMessage(MessageI m) throws Exception {
			((ReceptionCI)this.offering).acceptMessage(m);
		
	}

	@Override
	public void acceptMessages(MessageI[] ms) throws Exception{
		((ReceptionCI)this.offering).acceptMessages(ms);
		
	}

}
