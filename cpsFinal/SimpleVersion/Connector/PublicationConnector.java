package Connector;

import java.util.ArrayList;

import Message.MessageI;
import PublisherSubscriber.interfaces.PublicationCI;
import fr.sorbonne_u.components.connectors.AbstractConnector;


public class PublicationConnector  extends AbstractConnector implements PublicationCI{


	@Override
	public void publish(MessageI m, String topic) throws Exception{
		((PublicationCI)this.offering).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI m, String[] topic)throws Exception {
		((PublicationCI)this.offering).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI[] m, String[] topic) throws Exception{
		((PublicationCI)this.offering).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI[] m, String topic) throws Exception{
		((PublicationCI)this.offering).publish(m, topic);
		
	}



}
