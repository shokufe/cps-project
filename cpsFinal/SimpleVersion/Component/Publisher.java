package Component;


import java.util.ArrayList;


import Connector.ManagementBrokerPublisherConnector;
import Connector.PublicationConnector;
import Message.Message;
import Message.MessageI;
import Message.Properties;
import Message.TimeStamp;
import PublisherSubscriber.Port.ManagementInboundPort;
import PublisherSubscriber.Port.ManagementOutboundPort;
import PublisherSubscriber.Port.PublicationInboundPort;
import PublisherSubscriber.Port.PublicationOutboundPort;
import PublisherSubscriber.interfaces.ManagementCI;
import PublisherSubscriber.interfaces.PublicationCI;

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import fr.sorbonne_u.components.exceptions.PreconditionException;

import fr.sorbonne_u.components.ports.PortI;
/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */



@RequiredInterfaces(required = {PublicationCI.class,ManagementCI.class})
public class Publisher  extends AbstractComponent{

	protected PublicationInboundPort portInPub;
	protected PublicationOutboundPort portOutPub;
	protected ManagementInboundPort   portInManag;
	protected ManagementOutboundPort portOutManag;


	protected String publisherURI ;
	protected String publicationURI;
	protected String ManagementURI;
	ArrayList<String > topics;
	ArrayList<MessageI> messages=new ArrayList<>();
	
/**
 * Constructeur
 * @param uriPublisher
 * @param uriManag
 * @throws Exception
 */
	
	protected Publisher(String uriPublisher,String uriManag ) throws Exception{
	
		super(1,0) ;
		this.addRequiredInterface(ManagementCI.class);
		this.addRequiredInterface(PublicationCI.class);
		
		this.publicationURI = uriPublisher;
		this.ManagementURI=uriManag;
	
	
		
		//publicationCi port
	  portOutPub = new  PublicationOutboundPort(this) ;
	  portOutPub.localPublishPort();
	 
	   
	   //ManagementCI port 
	
	 portOutManag = new  ManagementOutboundPort(this) ;
	   portOutManag.localPublishPort();

		this.tracer.setTitle("publisher Component") ;
		this.tracer.setRelativePosition(1, 4) ;
	  
		
		
	
	}
	//methode start 

	@Override
	public void	start() throws ComponentStartException
	{
	
		
		try {
			this.traceMessage(
					"Connexion au Port du Publisher via l'interface Publication " ) ;
			
			// connexion avec le port du broker qui permet la publication des message 
			this.doPortConnection(
					this.portOutPub.getPortURI(), 
					this.publicationURI, 
					PublicationConnector.class.getCanonicalName()
					);
			
			this.traceMessage(
					"Publisher :Connexion au Port du Broker via l'interface Management " ) ;
			
			// connexion avec le port du  broker qui permet la traitance  des Messages 
			this.doPortConnection(
					this.portOutManag.getPortURI(), 
					this.ManagementURI, 
					ManagementBrokerPublisherConnector.class.getCanonicalName()
					);
			
		} catch (Exception e) {
			new ComponentStartException(e);
		}
		super.start();
	
	}
	
	
	//methode execute 
	
	@Override
	public void	execute() throws Exception
	{
		super.execute() ;

			this.traceMessage(
					"Publish messages"+ " " ) ;
			
			this.runTask(new AbstractComponent.AbstractTask() {
				@Override
				public void run() {
					((Publisher)this.getTaskOwner()).creationTopic("stl ");
				}
			});
			
			this.runTask(new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						((Publisher)this.getTaskOwner()).publish("publish");
					}
			});
			
			

		}
	
	
	/**
	 * Cette methode appel� par le publisher pour creer un topic
	 * @param string
	 */
	
	
	protected void creationTopic(String string) {
		this.traceMessage(" envoie du Message Cotennant comme properties (module ,stl)");
		this.runTask(
				new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						try {
							((Publisher)this.getTaskOwner()).portOutManag.createTopic("stl ");
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
	}
/**
 * Cette methode appel� par le publisher pour publier un message
 * @param m
 */
	public void	publish(String m) {
		this.traceMessage("le publisher  envoie le Message qui contient comme properties (module ,stl)");
		this.runTask(
				new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						Properties properties = new Properties();
						properties.putProp("module ", "stl ");
						TimeStamp time = new TimeStamp(10, "Debut Envoie ");
						MessageI e = new Message(publicationURI,time, properties, deployemetSimple.CVM.URI_MANAGEMENT);
				
						try {
							((Publisher)this.getTaskOwner()).portOutPub.publish(e, m);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
	}




	

	

}
