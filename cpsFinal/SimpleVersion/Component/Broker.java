package Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import Connector.ReceptionSubscriberBrokerConnector;
import Message.MessageFilterI;
import Message.MessageI;
import Message.Properties;
import PublisherSubscriber.Port.ManagementInboundPort;
import PublisherSubscriber.Port.ManagementOutboundPort;
import PublisherSubscriber.Port.PublicationInboundPort;
import PublisherSubscriber.Port.PublicationOutboundPort;
import PublisherSubscriber.Port.ReceptionIntboundPort;
import PublisherSubscriber.Port.ReceptionOutboundPort;
import PublisherSubscriber.interfaces.ManagementCI;
import PublisherSubscriber.interfaces.ManagementImplementation;
import PublisherSubscriber.interfaces.PublicationCI;
import PublisherSubscriber.interfaces.PublicationImplementationI;
import PublisherSubscriber.interfaces.ReceptionCI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.AbstractPlugin;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.exceptions.ComponentStartException;

/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */





@OfferedInterfaces(offered = {PublicationCI.class,ManagementCI.class})
@RequiredInterfaces(required = {ReceptionCI.class})

public class Broker extends AbstractComponent{

	private static final long serialVersionUID = 1L;
/**
 * declaration de ports
 */
	
	protected PublicationInboundPort portInPub;
	protected PublicationOutboundPort portOutPub;
	protected ManagementOutboundPort portOutManag;
	protected ManagementInboundPort portInManag;
	
	protected ReceptionOutboundPort portOutRec;
	protected ReceptionIntboundPort portInRec;
	

	

	  
 Map<String, Map<String,MessageFilterI >> topicSubscriber=new HashMap<String, Map<String,MessageFilterI >>();

protected  String portOutRec1;


protected String URIportSubscriber ;
/**
 * Constructeur
 * @param URIportPublication
 * @param URIportManagement
 * @param URIportManagement2
 * @param URIportSubscriber
 * @throws Exception
 */

protected Broker(String URIportPublication,String URIportManagement,String URIportManagement2, String URIportSubscriber) throws Exception {
	super(1,0);
	this.addOfferedInterface(ManagementCI.class);
	this.addOfferedInterface(PublicationCI.class);
	this.addRequiredInterface(ReceptionCI.class);
	

	
	this.URIportSubscriber=URIportSubscriber;
	
	this.portInPub = new PublicationInboundPort(URIportPublication,this);
	this.portInManag = new ManagementInboundPort(URIportManagement,this);
	this.portOutRec= new ReceptionOutboundPort(URIportManagement2,this);
	
	this.tracer.setTitle("BrokerComponent") ;
	this.tracer.setRelativePosition(0, 0) ;
	
	this.portInPub.publishPort();
	this.portInManag.publishPort();
	this.portOutRec.publishPort();
	
}


/*
 *  Component life-cycle
 */
/**
 * Satrt methode
 */
@Override
public void start() throws ComponentStartException {
	super.start();
	try {
		this.traceMessage(
				" Connection du subscriber au broker via le port portInPub" ) ;
		this.doPortConnection(
				this.portOutRec.getPortURI(),
				URIportSubscriber, 
			ReceptionSubscriberBrokerConnector.class.getCanonicalName());
	} catch (Exception e) {
		e.printStackTrace();
	}
}

@Override
public void finalise() throws Exception {

	this.portInManag.unpublishPort();
	this.portInPub.unpublishPort();
	this.portOutRec.unpublishPort();
	super.finalise();
}


/* 
 * 	Services 
 */
 
/**
 * Methode create Topic du publisher
 * @param topic
 * @throws Exception
 */

	public void createTopic(String topic) throws Exception
	
	{
   System.out.println("create topic");
    	 this.logMessage(" nouveau topic a publier "+topic+" \n");
 		topicSubscriber.put(topic,new  HashMap<String,MessageFilterI>());
    	
    
		
	}
/**
 * methode is topic  pour verifier si le topic existe
 * @param topic
 * @return
 * @throws Exception
 */

	public Boolean isTopic(String topic) throws Exception
	
	{
	
		return topicSubscriber.containsKey(topic);
}	
	
/**
 *  methode is getTopic pour recuperer tous les topics cr�es
 * @return
 * @throws Exception
 */

public String [] getTopics() throws Exception
	
	{
	Set<String> set=topicSubscriber.keySet(); 
	String[] liste = new String[set.size()];
	int i =0;
	for (String s : set) {
		liste[i++]=s;
	}
	return liste;
	}
/**
 
 *methode getPublicationPortURI pour recuperer le port de la publication
 * @return
 * @throws Exception
 */

public String getPublicationPortURI() throws Exception

{
	return this.portOutPub.getPortURI();
}

/**
 *  Methode create Topic du publisher
 * @param topics
 * @throws Exception
 */

public void createTopics(String[] topics) throws Exception
	
	{
		
		for ( String s : topics) {
			createTopic(s);
		 }
    }
		
	
/**
 * methode publish pour publier un message pourun topic
 * @param m
 * @param topic
 * @return
 * @throws Exception
 */

	public String publish(MessageI m,String topic) throws Exception
		
	 	{
		this.runTask(
				new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						try {
							((Broker)this.getTaskOwner()).portOutRec.acceptMessage(m);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				});	
		
	
		  
			return topic;

				   
				   
		}
	
	
/**
 *  methode publish pour publier un message pour plusieurs topics
 * @param m
 * @param topic
 * @return
 * @throws Exception
 */

public String publish(MessageI m,String[]topic) throws Exception

{
	this.runTask(
			new AbstractComponent.AbstractTask() {
				@Override
				public void run() {
					try {
						((Broker)this.getTaskOwner()).portOutRec.acceptMessage(m);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});	
	return m.getURI();
	
	}
/**
 *  methode publish pour publier des messages pour plusieurs topics
 * @param m
 * @param topic
 * @return
 * @throws Exception
 */

public String publish(MessageI[] m, String[]topic) throws Exception{
	
	this.runTask(
			new AbstractComponent.AbstractTask() {
				@Override
				public void run() {
					try {
						((Broker)this.getTaskOwner()).portOutRec.acceptMessages(m);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});	
    return "true";
	
}
/**
 *  methode publish pour publier plusieurs message pour un topic
 * @param m
 * @param topic
 * @return
 * @throws Exception
 */

public String publish(MessageI []m,String topic) throws Exception
	
{
    String uriMessage=null;
    this.runTask(
			new AbstractComponent.AbstractTask() {
				@Override
				public void run() {
					try {
						((Broker)this.getTaskOwner()).portOutRec.acceptMessages(m);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});	
				return uriMessage;
			   
	}
		

/**
 * methode de souscription a un topic 
 * @param topic
 * @param inboundPortURI
 * @return
 * @throws Exception
 */

public String subscribe(String topic,String inboundPortURI) throws Exception
	
	{
	this.logMessage(" j'ai recu une souscription de la part de " + inboundPortURI + " au topic : " + topic + " \n");
	if(topicSubscriber.containsKey(topic)) {
		topicSubscriber.get(topic).put(inboundPortURI,null);
	}else {
		createTopic(topic);
		subscribe(topic, inboundPortURI);
	}

	  return topic;
	}

	
	/**
	 * methode de souscription a plusieurs topics
	 * @param topics
	 * @param inboundPortURI
	 * @return
	 * @throws Exception
	 */
	
public String subscribe(String [] topics, String inboundPortURI) throws Exception{
		
	for ( String s : topics) {
		subscribe(s, inboundPortURI);
	}

		return inboundPortURI;
	}


/**
 * methode de souscription a un topic  par un filtre particulier
 * @param topic
 * @param filter
 * @param inboundPortURI
 * @return
 * @throws Exception
 */
		
	public String subscribe(String topic, MessageFilterI filter, String inboundPortURI) throws Exception{
		subscribe(topic, inboundPortURI);
		topicSubscriber.get(topic).put(inboundPortURI, filter);
		return null;
		
	}
		
	/**
	 * methode pour modifier le filtre d'un topic particulier
	 * @param topic
	 * @param newFilter
	 * @param inboundPortURI
	 * @return
	 * @throws Exception
	 */
public String modifyFilter(String topic, MessageFilterI newFilter, String inboundPortURI) throws Exception{
	if(topicSubscriber.containsKey(topic) && topicSubscriber.get(topic).containsKey(inboundPortURI))
		topicSubscriber.get(topic).put(inboundPortURI, newFilter);
		  return topic;


	}
		
		
		
		
/**
 * methode pour desinscrire d'un topic
 * @param topic
 * @param inboundPortURI
 */

public void unsubscribe(String topic,String inboundPortURI){
	if(this.topicSubscriber.containsKey(topic)) {
		topicSubscriber.get(topic).remove(inboundPortURI);
	}
	}	
		
		
		
	/**
	 * methode pour supprimer un topic	
	 * @param topic
	 * @throws Exception
	 */

public void destroyTopic(String topic) throws Exception

{
	topicSubscriber.remove(topic);
	
}
/**
 * methode port recuperer le port de publication
 * @return
 * @throws Exception
 */

public String getPublicationURI() throws Exception {
	return portInManag.getPortURI();
}


public void publish(ArrayList<MessageI> m, String[] topic) {
	// TODO Auto-generated method stub
	
}	



}
