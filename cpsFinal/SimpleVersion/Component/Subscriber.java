package Component;


import java.util.ArrayList;
import java.util.List;

import Connector.ManagementBrokerSubscriberConnector;
import Message.Filter;
import Message.Message;
import Message.MessageFilterI;
import Message.MessageI;
import Message.Properties;
import PublisherSubscriber.Port.ManagementInboundPort;
import PublisherSubscriber.Port.ManagementOutboundPort;
import PublisherSubscriber.Port.ReceptionIntboundPort;
import PublisherSubscriber.Port.ReceptionOutboundPort;
import PublisherSubscriber.interfaces.ManagementCI;
import PublisherSubscriber.interfaces.ReceptionCI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.exceptions.ComponentStartException;

/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */


@RequiredInterfaces(required= {ManagementCI.class})
@OfferedInterfaces(offered= {ReceptionCI.class})


public class Subscriber extends AbstractComponent {
	
	protected Subscriber(int nbThreads, int nbSchedulableThreads) {
		super(nbThreads, nbSchedulableThreads);
		// TODO Auto-generated constructor stub
	}



	protected ManagementOutboundPort portOutManag;
	protected ManagementInboundPort portInManag;
	
	
	protected ReceptionIntboundPort portInRec;
	protected ReceptionOutboundPort portOutRec;
	protected String subscriberURI;
	protected List<MessageI> Message  ;
	protected String managementURI;
	protected String receptionURI;

	/**
	 * Constructeur
	 * @param portInRec
	 * @param managementURI
	 * @throws Exception
	 */
	
	protected Subscriber(String portInRec,String managementURI) throws Exception   {
		super(1,0);
		
		this.addRequiredInterface(ManagementCI.class);
		this.addOfferedInterface(ReceptionCI.class);
		
		this.Message= new ArrayList<MessageI>();
		
		this.managementURI = managementURI;		
		this.portInRec = new ReceptionIntboundPort(portInRec,this);
		this.portOutManag = new ManagementOutboundPort(this);
		
		this.portOutManag.publishPort();
		this.portInRec.publishPort();
		
		this.tracer.setTitle("Subscriber Component") ;
		this.tracer.setRelativePosition(1,3) ;
	
	}
	
	

	@Override
	public void	start() throws ComponentStartException
	{
		try {
			this.traceMessage("le subscriber se connecte  au Port du Broker via l'interface ManagementCI " ) ;
			
			// connexion avec le port du  broker qui permet la traitance  des Messages 
			this.doPortConnection(
					this.portOutManag.getPortURI(), 
					this.managementURI, 
					ManagementBrokerSubscriberConnector.class.getCanonicalName()
					);
			
		
			
		} catch (Exception e) {
			new ComponentStartException(e);
		}
		super.start() ;

		
	}
	
	//methode execute du Subscriber 
	
	@Override
	public void	execute() throws Exception
	{
		super.execute() ;
	/*	String topic ="algav";
		String uri= "uri";
		String []topics = null;
		topics[0]="stl";
		topics[1]="dac";
		topics[2]="sar";
		
		MessageFilterI filter=new Filter();
		

			this.portOutManag.subscribe(topic, uri);
			this.portOutManag.subscribe(topics, uri);
			this.portOutManag.subscribe(topic, filter,uri);
			this.portOutManag.modifyFilter(topic, filter,uri);
			this.portOutManag.unsubscribe(topic, uri);*/
		this.traceMessage("Subscribe methode"+ " " ) ;
		
		this.runTask(new AbstractComponent.AbstractTask() {
			@Override
			public void run() {
				((Subscriber)this.getTaskOwner()).subscribeToTopic("algav", managementURI);
			}
		});
			
		
		}
	



	@Override
	public void	finalise() throws Exception
	{
		this.logMessage("stopping Subscriber component.") ;
		this.printExecutionLogOnFile("Subscriber");
		
		this.portOutManag.unpublishPort() ;

		super.finalise();
	}
	
/**
 * methode pour souscrire � un topic	
 * @param topic
 * @param inboundPortURI
 */
	public void subscribeToTopic(String topic, String inboundPortURI) {
		this.traceMessage("je m'abonne au topic : " + topic + "\n");
		this.runTask(
			new AbstractComponent.AbstractTask() {
				@Override
				public void run() {
					try {
						((Subscriber)this.getTaskOwner()).portOutManag.subscribe(topic, inboundPortURI);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});
	}

/**
 * methode du subscriber pour accepter un message recu qui a �t� publi� par le publisher
 * @param m
 */
	public void acceptMessage(MessageI m) {
		this.traceMessage("j'ai recu un message de la part de  "+m.getURI());
		System.out.println(m.getPayload());
		Message.add(m);
		
	}

	public void acceptMessages(MessageI[] ms){
		for (int i =0 ; i < ms.length ; i++ ) {
			acceptMessage(ms[i]);
		}
	}
		

}
