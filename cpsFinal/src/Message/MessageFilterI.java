package Message;
/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */

public interface MessageFilterI {
	/**
	 * 
	 * @param m
	 * @return
	 */
	
	boolean filter(MessageI m);
	boolean filterMap(MessageI m);

}
