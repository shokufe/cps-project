package Message;

import java.util.Map;
/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */

public class Filter implements MessageFilterI{
	
	protected Object[] filters;
	/**
	 * Constructeur
	 * @param map
	 */
	protected Map<String ,Object > filterss;
	/**
	 * Constructeur
	 * @param map
	 */
	
	public Filter(Map<String ,Object > map ) {
		this.filterss=map;
	}
	
	
	public Filter(Object[] map ) {
		this.filters=map;
	}
	/**
	 * methode filtrer qui associe a chaque message un filtre 
	 */

	@Override
	public boolean filter(MessageI m) {
		for(int i=0;i<filters.length;i++) {
		//for (String s:filters) {
			if ( ( filters[i] instanceof Integer  &&m.getProperties().getProperties().equals(filters[i]) || m.getProperties().getIntProp(filters[i].toString()) != ((Integer)filters[i]).intValue())) {
					return false;
				};
			if ( ( filters[i]instanceof Double && m.getProperties().getProperties().equals(filters[i])||m.getProperties().getDoubleProp(filters[i].toString()) != ((Double)filters[i]).doubleValue())) {
					return false;
			}
			if (  ( filters[i] instanceof String && m.getProperties().getStringProp(filters[i].toString())!=((String)filters[i]).toString())||m.getProperties().getProperties().containsKey(filters[i])) {
					return false;
			}
			if (  (filters[i] instanceof Byte && m.getProperties().getByteProp(filters[i].toString()) != ((Byte)filters[i]).byteValue())||m.getProperties().getProperties().containsKey(filters[i])) {
					return false;
			}
			if (  (filters[i] instanceof Boolean && m.getProperties().getBooleanProp(filters[i].toString()) != ((Boolean)filters[i]).booleanValue())||m.getProperties().getProperties().containsKey(filters[i])) {
				    return false;
			}
			if (  (filters[i] instanceof Long && m.getProperties().getlongProp(filters[i].toString()) != ((Long)filters[i]).longValue())||m.getProperties().getProperties().containsKey(filters[i])) {
					return false;
			}
			if (  (filters[i] instanceof String && m.getProperties().getCharProp(filters[i].toString()) != ((String)filters[i]).charAt(0))||m.getProperties().getProperties().containsKey(filters[i])) {
					return false;	
			}
			if (  (filters[i] instanceof Float && m.getProperties().getFloatProp(filters[i].toString()) != ((Float)filters[i]).floatValue())||m.getProperties().getProperties().containsKey(filters[i])) {
					return false;
			}
			if (  (filters[i] instanceof Short && m.getProperties().getShortProp(filters[i].toString())!= ((Short)filters[i]).shortValue())||m.getProperties().getProperties().containsKey(filters[i])) {
					return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean filterMap(MessageI m) {
		for (String s:filterss.keySet()) {
			if ( ( filterss.get(s) instanceof Integer  && m.getProperties().getIntProp(s) != ((Integer)filterss.get(s)).intValue())||m.getProperties().getProperties().containsKey(s) ) {
					return false;
				};
			if ( (filterss.get(s) instanceof Double &&m.getProperties().getDoubleProp(s) != ((Double)filterss.get(s)).doubleValue())||m.getProperties().getProperties().containsKey(s)) {
					return false;
			}
			if (  (filterss.get(s) instanceof String && !m.getProperties().getStringProp(s).equals((String)filterss.get(s).toString())||m.getProperties().getProperties().containsKey(s))) {
					return false;
			}
			if (  (filterss.get(s) instanceof Byte && m.getProperties().getByteProp(s) != ((Byte)filterss.get(s)).byteValue())||m.getProperties().getProperties().containsKey(s)) {
					return false;
			}
			if (  (filterss.get(s) instanceof Boolean && m.getProperties().getBooleanProp(s) != ((Boolean)filterss.get(s)).booleanValue())||m.getProperties().getProperties().containsKey(s)) {
				    return false;
			}
			if (  (filterss.get(s) instanceof Long && m.getProperties().getlongProp(s) != ((Long)filterss.get(s)).longValue())||m.getProperties().getProperties().containsKey(s)) {
					return false;
			}
			if (  (filterss.get(s) instanceof String && m.getProperties().getCharProp(s) != ((String)filterss.get(s)).charAt(0))||m.getProperties().getProperties().containsKey(s)) {
					return false;	
			}
			if (  (filterss.get(s) instanceof Float && m.getProperties().getFloatProp(s) != ((Float)filterss.get(s)).floatValue())||m.getProperties().getProperties().containsKey(s)) {
					return false;
			}
			if (  (filterss.get(s) instanceof Short && m.getProperties().getShortProp(s)!= ((Short)filterss.get(s)).shortValue())||m.getProperties().getProperties().containsKey(s)) {
					return false;
			}
		}
		return true;
	}
	
	
	
	
	
}
	
