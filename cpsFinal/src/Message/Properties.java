package Message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */

public class Properties implements Serializable{
	

	private static final long serialVersionUID = 1L;
	private Map<String , Object > properties ;
	
	public Properties() {
		this.properties = new HashMap<String,Object>();
	}
	
	public Map<String , Object > getProperties(){
		return properties;
	}
	
	
	public boolean existProp(String prop2) {
		
		return properties.containsKey(prop2);
	}
	public void putProp(String nom , boolean b) {
		properties.put(nom, b);
	}
	
	public void putProp(String nom , byte b) {
		properties.put(nom, b);
	}
	
	public void putProp(String nom , char b) {
		properties.put(nom, b);
	}
	
	public void putProp(String nom , double b) {
		properties.put(nom, b);
	}
	
	public void putProp(String nom , float b) {
		properties.put(nom, b);
	}

	public void putProp(String nom , int  b) {
		properties.put(nom, b);
	}

	public void putProp(String nom ,long b) {
		properties.put(nom, b);
	}
	
	public void putProp(String nom , short b) {
		properties.put(nom, b);
	}

	public void putProp(String nom , String b) {
		properties.put(nom, b);
	}
	
	public boolean getBooleanProp(String nom) {
		return (boolean)properties.get(nom);
	}
	
	public long getlongProp(String nom) {
		return (long)properties.get(nom);
	}
	
	public byte getByteProp(String nom) {
		return (byte)properties.get(nom);
	}
	
	public short getShortProp(String nom) {
		return (short )properties.get(nom);
	}
	
	public char getCharProp(String nom) {
		return (char)properties.get(nom);
	}
	
	public int getIntProp(String nom) {
		return (int)properties.get(nom);
	}
	
	public String getStringProp(String nom) {
		return (String)properties.get(nom);
	}
	
	public double getDoubleProp(String nom) {
		return (double)properties.get(nom);
	}
	
	public float getFloatProp(String nom) {
		return (float)properties.get(nom);
	}
	


}
