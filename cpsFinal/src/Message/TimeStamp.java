package Message;

import java.sql.Timestamp;
import java.util.Date;
/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */

public class TimeStamp {
	
	public long time;
	public String timeStamp;
	
/**
 * 
 * @param time
 * @param timeStamp
 */
 public TimeStamp(long time, String timeStamp) {
		super();
		this.time = time;
		this.timeStamp = timeStamp;
	}

public boolean isInitialised() {
	 return false;
 }
/**
 * methode pour recuperer le time	 
 * @return
 */
	
	public long getTime() {
		 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		time= timestamp.getTime();
		return time;
	}
/**
 * methode pour modifier le time	
 * @param time
 */
	
	public void setTime(long time) {
		this.time = time;
	}
	
/**
 * recuperer le timeStamp	
 * @return
 */
	public String getTimeStamp() {
		Date date=new Date();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		 String t=Long.toString(timestamp.getTime());
		TimeStamp tmps=new TimeStamp(date.getTime(),t);
		return tmps.toString();
	}
	
	
	public TimeStamp() {
		super();
	}

/**
 * methode pour modifier le time stamp
 * @param timeStamp
 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	

}
