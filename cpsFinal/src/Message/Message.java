package Message;

import java.io.Serializable;

/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */

public class Message implements MessageI {

/**
 * 
 */
	private static final long serialVersionUID = 1L;
	protected Properties proprties ;
	protected TimeStamp t;
	protected String id;
	protected Serializable contenu;
	
	/**
	 * Constructeur
	 * @param id
	 * @param t
	 * @param proprties
	 * @param contenu
	 */
	public Message(String id,TimeStamp t , Properties proprties, Serializable contenu) {
		this.id = id;
		this.t=t;
		this.proprties=proprties;
		this.contenu = contenu;
	}

	public Message(TimeStamp time, Properties properties,String m) {
		this.t=time;
		this.proprties=properties;
		this.contenu=m;
		
	}

	@Override
	public String getURI() {
		return this.id;
	}

	@Override
	public TimeStamp getTimeStaps() {
		return this.t;
	}

	@Override
	public Serializable getPayload() {
		return this.contenu;
	}

	@Override
	public Properties getProperties() {
		return this.proprties;
	}

}
