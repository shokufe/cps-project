package Message;
import java.io.Serializable;


import java.io.Serializable;

/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */

public interface MessageI extends Serializable{

	public String getURI();
	public TimeStamp getTimeStaps();
	public Serializable getPayload();
	public Properties getProperties();
}
