package Component.plug;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import Connector.ReceptionSubscriberBrokerConnector;
import Message.MessageFilterI;
import Message.MessageI;
import PublisherSubscriber.Port.ManagementInboundPort;
import PublisherSubscriber.Port.ManagementOutboundPort;
import PublisherSubscriber.Port.PublicationInboundPort;
import PublisherSubscriber.Port.PublicationOutboundPort;
import PublisherSubscriber.Port.ReceptionIntboundPort;
import PublisherSubscriber.Port.ReceptionOutboundPort;
import PublisherSubscriber.interfaces.ManagementCI;
import PublisherSubscriber.interfaces.ManagementImplementation;
import PublisherSubscriber.interfaces.PublicationCI;
import PublisherSubscriber.interfaces.PublicationImplementationI;
import PublisherSubscriber.interfaces.ReceptionCI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.AbstractPlugin;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.exceptions.ComponentStartException;

/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */



@OfferedInterfaces(offered = {PublicationCI.class,ManagementCI.class})
@RequiredInterfaces(required = {ReceptionCI.class})

public class Broker extends AbstractPlugin{

	private static final long serialVersionUID = 1L;

	
	protected PublicationInboundPort portInPub;
	protected PublicationOutboundPort portOutPub;
	protected ManagementOutboundPort portOutManag;
	protected ManagementInboundPort portInManag;
	
	protected ReceptionOutboundPort portOutRec;
	protected ReceptionIntboundPort portInRec;
	

	

	  
 Map<String, Map<String,MessageFilterI >> topicSubscriber=new HashMap<String, Map<String,MessageFilterI >>();

protected  String portOutRec1;


 
	@Override
	public void	installOn(ComponentI owner) throws Exception
	{
		super.installOn(owner) ;

		assert	owner instanceof ManagementImplementation ;
		assert	owner instanceof PublicationImplementationI ;


		// Add interfaces and create ports
		this.addOfferedInterface(ManagementCI.class) ;
		this.addOfferedInterface(PublicationCI.class) ;
		this.addRequiredInterface(PublicationCI.class) ;

		this.portInManag = new ManagementInboundPort(
									this.getPluginURI(), this.owner) ;
		this.portInManag.publishPort() ;
		this.portInPub = new PublicationInboundPort(
				this.getPluginURI(), this.owner) ;
       this.portInPub.publishPort() ;
       
       this.portOutRec = new ReceptionOutboundPort(
				this.getPluginURI(), this.owner) ;
      this.portOutRec.publishPort() ;
     
	}
	
	@Override
	public void	initialise() throws Exception
	{
		 try {
				this.logMessage(
						"se connecter  au Port du subscriber via l'interface Reception  " ) ;
				this.owner.doPortConnection(
						this.portOutRec.getPortURI(),
						portOutRec1, 
						ReceptionSubscriberBrokerConnector.class.getCanonicalName());
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("apres start");

		super.initialise();
	}
 
	@Override
	public void	uninstall() throws Exception
	{
		this.portInManag.unpublishPort() ;
		this.portInManag.destroyPort() ;
		
		this.portInPub.unpublishPort() ;
		this.portInPub.destroyPort() ;
		
		this.portOutRec.unpublishPort() ;
		this.portOutRec.destroyPort() ;
		this.removeOfferedInterface(ManagementCI.class) ;
		this.removeOfferedInterface(PublicationCI.class) ;
		this.removeRequiredInterface(ReceptionCI.class) ;


	}


	@SuppressWarnings({ "unchecked" })
	private ManagementImplementation getOwnerManag()
	{
		return ((ManagementImplementation)this.owner) ;
	}
	
	@SuppressWarnings({ "unchecked" })
	private PublicationImplementationI getOwnerPub()
	{
		return ((PublicationImplementationI)this.owner) ;
	}
	




	

	//methode du Broker createTopic

	public void createTopic(String topic) throws Exception
	
	{
   System.out.println("create topic");
    	 this.logMessage(" nouveau topic a publier "+topic+" \n");
 		topicSubscriber.put(topic,new  HashMap<String,MessageFilterI>());
    	
    
		
	}
//	
//	//methode du Broker createTopic

	public Boolean isTopic(String topic) throws Exception
	
	{
	
		return topicSubscriber.containsKey(topic);
}	
	
	

public String [] getTopics() throws Exception
	
	{
	Set<String> set=topicSubscriber.keySet(); 
	String[] liste = new String[set.size()];
	int i =0;
	for (String s : set) {
		liste[i++]=s;
	}
	return liste;
	}

public String getPublicationPortURI() throws Exception

{
	return this.portOutPub.getPortURI();
}

	//methode du Broker createTopic

public void createTopics(String[] topics) throws Exception
	
	{
		
		for ( String s : topics) {
			createTopic(s);
		 }
    }
		
	
	
	//methode du publish

	public String publish(MessageI m,String topic) throws Exception
		
	 	{
		this.getOwnerPub().publish(m, topic);
		
	
		  
			return topic;

				   
				   
		}
	
	

	
	//methode du publish plusieur topics pour un meme message

public String publish(MessageI m,String[]topic) throws Exception

{
	this.getOwnerPub().publish(m, topic);
	return m.getURI();
	
	}

public String publish(MessageI[] m, String[]topic) throws Exception{
	

	this.getOwnerPub().publish(m, topic);
    return "true";
	
}

//methode du publish plusieurs messages pour un topic

public String publish(MessageI []m,String topic) throws Exception
	
{
    String uriMessage=null;
	 
    this.getOwnerPub().publish(m, topic);
				return uriMessage;
			   
	}
		


	//methode du Broker subscriber

public String subscribe(String topic,String inboundPortURI) throws Exception
	
	{
	this.logMessage(" j'ai recu une souscription de la part de " + inboundPortURI + " au topic : " + topic + " \n");
	if(topicSubscriber.containsKey(topic)) {
		topicSubscriber.get(topic).put(inboundPortURI,null);
	}else {
		createTopic(topic);
		subscribe(topic, inboundPortURI);
	}

	  return topic;
	}

	
	
	
public String subscribe(String [] topics, String inboundPortURI) throws Exception{
		
	for ( String s : topics) {
		subscribe(s, inboundPortURI);
	}

		return inboundPortURI;
	}
		
	public String subscribe(String topic, MessageFilterI filter, String inboundPortURI) throws Exception{
		subscribe(topic, inboundPortURI);
		topicSubscriber.get(topic).put(inboundPortURI, filter);
		return null;
		
	}
		
	
public String modifyFilter(String topic, MessageFilterI newFilter, String inboundPortURI) throws Exception{
	if(topicSubscriber.containsKey(topic) && topicSubscriber.get(topic).containsKey(inboundPortURI))
		topicSubscriber.get(topic).put(inboundPortURI, newFilter);
		  return topic;


	}
		
		
		
		


public void unsubscribe(String topic,String inboundPortURI){
	if(this.topicSubscriber.containsKey(topic)) {
		topicSubscriber.get(topic).remove(inboundPortURI);
	}
	}	
		
		
		
		

public void destroyTopic(String topic) throws Exception

{
	topicSubscriber.remove(topic);
	
}

public String getPublicationURI() throws Exception {
	return portInManag.getPortURI();
}	



}
