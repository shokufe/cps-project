package Component.plug;




import Connector.PublicationConnector;
import Message.MessageI;
import PublisherSubscriber.Port.PublicationOutboundPort;
import PublisherSubscriber.interfaces.PublicationCI;
import deployementThread.CVMThread;
import fr.sorbonne_u.components.AbstractPlugin;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.PluginI;
import fr.sorbonne_u.components.reflection.connectors.ReflectionConnector;
import fr.sorbonne_u.components.reflection.interfaces.ReflectionI;
import fr.sorbonne_u.components.reflection.ports.ReflectionOutboundPort;



/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */

public class publisherPlugin extends AbstractPlugin implements PublicationCI,PluginI {
	/*
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	
	protected PublicationOutboundPort portOutPub;
	
	
	
	@Override
	public void installOn(ComponentI owner) throws Exception {
		System.out.println("Plugin Install");
		this.addRequiredInterface(PublisherSubscriber.interfaces.ReceptionCI.class);
		
		this.portOutPub = new PublicationOutboundPort(this.owner);
		
		this.portOutPub.publishPort();
		
		
		super.installOn(owner);
	}
	
	@Override
	public void initialise() throws Exception {
		System.out.println("Plugin Init");
		this.addRequiredInterface(ReflectionI.class);
		ReflectionOutboundPort rop = new ReflectionOutboundPort(this.owner) ;
		rop.publishPort() ;
		
		this.owner.doPortConnection(
				rop.getPortURI(),
				CVMThread.URI__PUBLISHER,
				ReflectionConnector.class.getCanonicalName()) ;
		
		String[] uris = rop.findPortURIsFromInterface(PublisherSubscriber.interfaces.ReceptionCI.class);
		assert	uris != null && uris.length == 1 ;

		this.owner.doPortDisconnection(rop.getPortURI()) ;
		rop.unpublishPort() ;
		rop.destroyPort() ;
		this.removeRequiredInterface(ReflectionI.class) ;
		
		// connect the outbound port.
		this.owner.doPortConnection(
				this.portOutPub.getPortURI(),
				uris[0],
				PublicationConnector.class.getCanonicalName()) ;

		super.initialise();
	}
	
	public publisherPlugin() {
		super();
	}

	public publisherPlugin(PublicationOutboundPort portPublication) {
		super();
		this.portOutPub = portPublication;
	}

	@Override
	public void finalise() throws Exception {
		this.owner.doPortDisconnection(this.portOutPub.getPortURI());
		super.finalise();
	}
	
	@Override
	public void uninstall() throws Exception {
		this.portOutPub.unpublishPort() ;
		this.portOutPub.destroyPort() ;
		this.removeRequiredInterface(PublicationCI.class) ;
		super.uninstall();
	}
	
	
/**
 * plugin publisher service 
 */
	@Override
	public void publish(MessageI m, String topic) throws Exception {
		System.out.println("publish message");
		this.portOutPub.publish(m, topic);
		
	}

	@Override
	public void publish(MessageI m, String[] topics) throws Exception {
		this.portOutPub.publish(m, topics);
		
	}

	@Override
	public void publish(MessageI[] ms, String topic) throws Exception {
		this.portOutPub.publish(ms, topic);
	}

	@Override
	public void publish(MessageI[] ms, String[] topics) throws Exception {
		this.portOutPub.publish(ms, topics);
	}
	
}
