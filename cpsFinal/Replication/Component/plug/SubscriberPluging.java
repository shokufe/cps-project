package Component.plug;


import Message.MessageI;

import PublisherSubscriber.Port.ReceptionIntboundPort;
import PublisherSubscriber.interfaces.ManagementCI;
import PublisherSubscriber.interfaces.ReceptionCI;
import PublisherSubscriber.interfaces.ReceptionImplementationI;
import fr.sorbonne_u.components.AbstractPlugin;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ReflectionInboundPort;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.reflection.interfaces.ReflectionI;



/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */



@RequiredInterfaces(required= {ManagementCI.class})
@OfferedInterfaces(offered= {ReceptionCI.class})

public class SubscriberPluging  extends AbstractPlugin implements ReceptionImplementationI {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected ReceptionIntboundPort portInRec;
	
	@Override
	public void installOn(ComponentI owner) throws Exception {
		
		super.installOn(owner);
		this.addOfferedInterface(ReceptionCI.class);
		this.portInRec = new ReceptionIntboundPort(this.getPluginURI(), this.owner);
		this.portInRec.publishPort();
		
	}
		@Override
	public void initialise() throws Exception {
		this.addRequiredInterface(ReflectionI.class);
		ReflectionInboundPort rop = new ReflectionInboundPort(this.owner);
		rop.publishPort() ;
		super.initialise();
	}
	
	@Override
	public void finalise() throws Exception {
		super.finalise();
	}
	
	@Override
	public void uninstall() throws Exception {
		this.portInRec.unpublishPort() ;
		this.portInRec.destroyPort() ;
		this.removeOfferedInterface(ReceptionCI.class) ;
		super.uninstall();
	}
	

	@Override
	public void acceptMessage(MessageI m) throws Exception {
		System.out.println("accept");
		this.portInRec.acceptMessage(m);
	}

	@Override
	public void acceptMessages(MessageI[] ms) throws Exception {
		this.portInRec.acceptMessages(ms);
	}

}
