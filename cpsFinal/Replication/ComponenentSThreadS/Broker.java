package ComponenentSThreadS;

import java.rmi.Naming;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import Message.Filter;
import Message.MessageFilterI;
import Message.MessageI;
import connectors.ManagementBrokerSubscriberConnector;
import connectors.PublicationConnector;
import connectors.ReceptionSubscriberBrokerConnector;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.alasca.replication.interfaces.ManagementCI;
import fr.sorbonne_u.alasca.replication.interfaces.PublicationCI;
import fr.sorbonne_u.alasca.replication.interfaces.ReceptionCI;
import fr.sorbonne_u.alasca.replication.interfaces.RemoteInterface;

import fr.sorbonne_u.alasca.replication.ports.ManagementInboundPort;
import fr.sorbonne_u.alasca.replication.ports.PublicationInboundPort;
import fr.sorbonne_u.alasca.replication.ports.ReceptionOutboundPort;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.AbstractComponent.AbstractTask;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;


/**
 * Composant broker qui represente le server de notre systeme c'est lui qui gere les connection entre le publisher et le subscriber .
 * Derbene,sh ahmadi simab
 */

public class Broker extends AbstractComponent {

	
	/** URI de threads pools de Publish	    */
	public static final String			Publish_MESSAGES_HANDLER_URI            = "publishMessageToSubscribers" ;
	/** URI de threads pools de l'envoie des messages aux abonnés		    */
	public static final String			FILTER_MESSAGES_HANDLER_URI            = "filtrerLesMessages" ;
	/** URI de threads pools de sauvegarde des messages publier par le publisher              */
	public static final String			MESSAGES_PUBLISHER_HANDLER_URI   = "messagepublier" ;
	/** URI de threads pools de sauvegardes des suscriptions aux topics     */
	public static final String			SUBSCRIPTION_TOPICS_HANDLER_URI = "topicsSubscriber" ;
	
	/*
	 * ********************Declaration de variables***************************
	 */

	
	protected ManagementInboundPort portInManag ;
	
	/*map qui contient les topics associ�s a leurs messages qui sont a leurs tour associ�s � un filtre*/
protected Map<String,Map<String,MessageFilterI>> topicsSubscriber;
protected Map<String,Object> topicss;


	protected final Condition hashMapLockMessageAenvoyerCondition;

	
	/*un verou pour gerer lesdifferents acc�s a nos maps*/
	protected Map<String,ReentrantReadWriteLock> lockMap ;
	
	/* Liste des ports de subscribers */
	protected HashMap<String, ReceptionOutboundPort>   subscriberPorts;
	
	/* un verrou sur cette map pour gerer les acc�s*/
	protected final ReentrantReadWriteLock	subscriberPortsLock ;
	
	/* Liste des differents publisher Ports*/
	protected List<PublicationInboundPort> listePublisherPort;
	
	/* un verou pour gerer les differents acc�es � nos messages publi�s*/
	protected final ReentrantReadWriteLock	lockPublishedMessages ;
	
	/* la map de masses a publier */
	protected HashMap<String,List< MessageI> >  publishedMessages;
	
	protected String URIportSubscriber;
	MessageI [] message;
/**
 * Constructeur
 * @param URIportManagement
 * @param nbThreadsReceivers
 * @param nbThreadsBroker
 * @param nbThreadsPublisher
 * @param nbThreadsfilters
 * @throws Exception
 */

	protected Broker(String uri,
			String URIportManagement,
					 int    nbThreadsReceivers,
					 int    nbThreadsBroker,
					 int	nbThreadsPublisher,
					 int    nbThreadsfilters) throws Exception 
	{
		
		
		super(uri, 1, 0) ;
		this.addOfferedInterface(ManagementCI.class);
		this.addOfferedInterface(PublicationCI.class);
		this.addRequiredInterface(ReceptionCI.class);
		
		/* --------------------------------------------------*/
	/* map  des topics associ�s � laurs messages qui sont � leurs tour assoc� a des filtres*/
		this.topicsSubscriber = new HashMap<String, Map<String , MessageFilterI> >();
		
		/*le verrou qui gere les differents acc�s aux maps*/
		this.lockMap = new ConcurrentHashMap<String,ReentrantReadWriteLock>();
		this.publishedMessages= new HashMap<>();
		this.lockPublishedMessages = new ReentrantReadWriteLock();

		this.createNewExecutorService(SUBSCRIPTION_TOPICS_HANDLER_URI, nbThreadsReceivers, false);
		this.createNewExecutorService(MESSAGES_PUBLISHER_HANDLER_URI, nbThreadsBroker, false);
		this.createNewExecutorService(Publish_MESSAGES_HANDLER_URI, nbThreadsPublisher, false);
		this.createNewExecutorService(FILTER_MESSAGES_HANDLER_URI, nbThreadsfilters, false);
		
		
		this.hashMapLockMessageAenvoyerCondition = lockPublishedMessages.writeLock().newCondition();
		this.subscriberPortsLock = new ReentrantReadWriteLock();
		
		this.subscriberPorts = new HashMap<String, ReceptionOutboundPort>();
		this.listePublisherPort = new ArrayList<PublicationInboundPort>();
	

		this.portInManag = new ManagementInboundPort(
				this.getExecutorServiceIndex(SUBSCRIPTION_TOPICS_HANDLER_URI),
				URIportManagement,
				this
				);
		this.portInManag.publishPort();
// controle de type de connection 
		
		if (AbstractCVM.isDistributed) {
			System.out.println("non local");
			this.executionLog.setDirectory(System.getProperty("user.dir")) ;
		} else {
			//System.out.println(">>>local");
			this.executionLog.setDirectory(System.getProperty("user.home")) ;
			System.out.println(">>> after local");
		}

		this.tracer.setTitle("Broker") ;
		this.tracer.setRelativePosition(1, 0);
	}
	
	
	
	/**
	 * Constructor
	 * @param uri
	 * @param URIportManagement
	 * @throws Exception
	 */
	
	protected Broker(String uri,String URIportManagement) throws Exception 
	{
		
		super(uri,1,0) ;
		System.out.println("debut broker ");
		this.addOfferedInterface(BrokerCI.class);
	
		this.subscriberPortsLock = new ReentrantReadWriteLock();
		this.lockPublishedMessages = new ReentrantReadWriteLock();
		
		this.hashMapLockMessageAenvoyerCondition = null;

	System.out.println("broker const");

		this.portInManag = new ManagementInboundPort(
				URIportManagement,
				this
				);
		this.portInManag.publishPort();
		
		if (AbstractCVM.isDistributed) {
			System.out.println("non local");
			this.executionLog.setDirectory(System.getProperty("user.dir")) ;
		} else {
			//System.out.println(">>>local");
			this.executionLog.setDirectory(System.getProperty("user.home")) ;
			System.out.println(">>> after local");
		}

		this.tracer.setTitle("Broker") ;
		this.tracer.setRelativePosition(0, 0);
	}
	
	
	
/**
 * Start methode ou on fait appel au methode pour repondre au publisher et au subscriber
 */

	@Override
	public void start() throws ComponentStartException {
		this.logMessage("Broker");
		

		super.start();
	

		
	}
	
	@Override
	public void execute() throws Exception {
		       

			this.runTask(new AbstractComponent.AbstractTask() {
				@Override
				public void run() {
					try {
						
						((Broker)this.getTaskOwner()).publishMessage();

					} catch (Exception e) {
						new RuntimeException(e);
					}
				}
			});
		//	this.traceMessage("accepte message "+ m+"\n");
			
			  this.logMessage("ecexute");
		super.execute();
		
	}

/**
 * finalise methode
 */
	@Override
	public void finalise() throws Exception {
		this.portInManag.unpublishPort();
		super.finalise();
	}

/**
 * publier message 
 */
		
	private void publishMessage() throws Exception {

		lockPublishedMessages.writeLock().lock();
	//	this.logMessage(">>>>"+publishedMessages.size());
		for (String port :publishedMessages.keySet()) {
			this.logMessage("accepte message");
			 message = new MessageI[publishedMessages.get(port).size()]; 
			for (int i = 0 ; i<message.length;i++)		
				message[i]= publishedMessages.get(port).get(i);
			if (message.length>1) {
			
				this.subscriberPorts.get(port).acceptMessages(message);
				this.logMessage("accepte message"+message);
			} else {
				this.subscriberPorts.get(port).acceptMessage(message[0]);
				this.traceMessage("accepte message"+message[0]);
			}
		}

		publishedMessages.clear();
		lockPublishedMessages.writeLock().unlock();
	}	
	
	


	/**
	 * methode pour publier plusieurs message pour un topic
	 * @param topic
	 * @param ms
	 * @throws Exception
	 */
	private void publishMessage(String topic,MessageI[] ms) throws Exception{
		lockMap.get(topic).writeLock().lock();
		Map<String,MessageFilterI> topics = topicsSubscriber.get(topic);
		lockMap.get(topic).writeLock().unlock();
		
		for (String top : topics.keySet()) {
			for (MessageI m : ms) {
				lockPublishedMessages.writeLock().lock();
				
				if ( (topics.get(top)!= null && topics.get(top).filter(m))) {
					if (!publishedMessages.containsKey(top)) {
						publishedMessages.put(top, new ArrayList<MessageI>());
					}
					publishedMessages.get(top).add(m);
				}					
				lockPublishedMessages.writeLock().unlock();
				
					
			}
		}
		this.runTask(
				Publish_MESSAGES_HANDLER_URI,
				new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						try {
								((Broker)this.getTaskOwner()).publishMessage();
							
						} catch (Exception e) {
							new RuntimeException(e);
						}
					}
				});
	}
	
	/**
	 * 
	 * @param topic
	 * @param m
	 * @throws Exception
	 */
	private void publishMessage(String topic,MessageI m) throws Exception{
		lockMap.get(topic).writeLock().lock();
		Map<String,MessageFilterI> topics = topicsSubscriber.get(topic);
		lockMap.get(topic).writeLock().unlock();
		
		for (String top : topics.keySet()) {
			lockPublishedMessages.writeLock().lock();
			
			if ( (topics.get(top)!= null && topics.get(top).filter(m))) {
				if (!publishedMessages.containsKey(top)) {
					publishedMessages.put(top, new ArrayList<MessageI>());
				}
				publishedMessages.get(top).add(m);
			}				
			lockPublishedMessages.writeLock().unlock();
			
		}
		this.runTask(
				Publish_MESSAGES_HANDLER_URI,
				new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						try {
								((Broker)this.getTaskOwner()).publishMessage();
							
						} catch (Exception e) {
							new RuntimeException(e);
						}
					}
				});
	}
	/**
	 * 
	 * @param t
	 * @param m
	 * @throws Exception
	 */
	private void publishMessage(String[] t,MessageI m) throws Exception{
		for (String topic:t) {
		lockMap.get(topic).writeLock().lock();
		Map<String,MessageFilterI> topics = topicsSubscriber.get(topic);
		lockMap.get(topic).writeLock().unlock();
		
		for (String top : topics.keySet()) {
			lockPublishedMessages.writeLock().lock();
				
				if ( (topics.get(top)!= null && topics.get(top).filter(m))) {
					if (!publishedMessages.containsKey(top)) {
						publishedMessages.put(top, new ArrayList<MessageI>());
					}
					publishedMessages.get(top).add(m);
				}					
				lockPublishedMessages.writeLock().unlock();
				
			}
		}
		this.runTask(
				Publish_MESSAGES_HANDLER_URI,
				new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						try {
								((Broker)this.getTaskOwner()).publishMessage();
							
						} catch (Exception e) {
							new RuntimeException(e);
						}
					}
				});
	}
	/** publier plusieurs messages sur plueurs topics
	 * 
	 * @param t
	 * @param ms
	 * @throws Exception
	 */
	private void publishMessage(String[] t,MessageI[] ms) throws Exception{
		for (String topic:t) {
		lockMap.get(topic).writeLock().lock();
		Map<String,MessageFilterI> topics = topicsSubscriber.get(topic);
		lockMap.get(topic).writeLock().unlock();
		for (MessageI m : ms) {
			for (String top : topics.keySet()) {
				lockPublishedMessages.writeLock().lock();
				
				if ( (topics.get(top)!= null && topics.get(top).filter(m))) {
					if (!publishedMessages.containsKey(top)) {
						publishedMessages.put(top, new ArrayList<MessageI>());
					}
					publishedMessages.get(top).add(m);
				}
				lockPublishedMessages.writeLock().unlock();
				
				}
			}
		}
		this.runTask(
				Publish_MESSAGES_HANDLER_URI,
				new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						try {
								((Broker)this.getTaskOwner()).publishMessage();
							
						} catch (Exception e) {
							new RuntimeException(e);
						}
					}
				});
	}
/**
 * creer une map de subscriber 
 * @param inboundPortURI
 * @throws Exception
 */

	public void MapofSubscribers(String inboundPortURI) throws Exception {
		if( !this.subscriberPorts.containsKey(inboundPortURI)) {
			ReceptionOutboundPort port = new ReceptionOutboundPort(this);
			port.publishPort();
			this.doPortConnection(
					port.getPortURI(), 
					inboundPortURI, 
					ReceptionSubscriberBrokerConnector.class.getCanonicalName()
			);
			
			this.subscriberPorts.put(inboundPortURI, port);
		}
	}
	
/**
 * publier un message sur un topic particulier
 * @param m
 * @param topic
 * @throws Exception
 */
	public void publish(MessageI m,String topic)throws Exception {
		this.traceMessage("le message " + m.getURI() + ", qui a comme  topic : " + topic +" "+  " a �t� publi� ");
		this.runTask(FILTER_MESSAGES_HANDLER_URI,new AbstractTask() {
			@Override
			public void run() {
				try {
					((Broker)this.getTaskOwner()).publishMessage(topic,m);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
		
	}		
	
/**
 * publier plusieurs messages sur un topic
 * @param ms
 * @param topic
 * @throws Exception
 */
	public void Publish(MessageI[] ms, String topic)throws Exception {
		this.runTask(FILTER_MESSAGES_HANDLER_URI,new AbstractTask() {
			@Override
			public void run() {
				try {
					((Broker)this.getTaskOwner()).publishMessage(topic,ms);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}

/**
 * publier un message dans plusieurs topics
 * @param m
 * @param topics
 * @throws Exception
 */
	public void Publish(MessageI m, String[] topics)throws Exception {
		this.runTask(FILTER_MESSAGES_HANDLER_URI,new AbstractTask() {
			@Override
			public void run() {
				try {
					((Broker)this.getTaskOwner()).publishMessage(topics,m);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}

/**
 * publier plusieurs messages sur plusieurs topics
 * @param ms
 * @param topics
 * @throws Exception
 */
	public void Publish(MessageI[] ms, String[] topics)throws Exception {
		this.runTask(FILTER_MESSAGES_HANDLER_URI,new AbstractTask() {
			@Override
			public void run() {
				try {
					((Broker)this.getTaskOwner()).publishMessage(topics,ms);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});

	} 
	
/**
 * creation de topic en controllant les differents acc�es concurents
 * @param topic
 * @throws Exception
 */
	public void createTopic(String topic)throws Exception {
		this.traceMessage(" le topic  " + topic + " a �t� cr�e");
		lockMap.put(topic,new ReentrantReadWriteLock());
		lockMap.get(topic).writeLock().lock();
		topicsSubscriber.put(topic,new  HashMap<String,MessageFilterI>());
		topicss.put(topic, topic);
		lockMap.get(topic).writeLock().unlock();;
	}
	

/**
 * 	creation de plusieurs topics
 * @param topic
 * @throws Exception
 */
	public void createTopic(String[] topic) throws Exception{
		for ( String s : topic) {
			createTopic(s);
		}
	}
	
/**
 * la destruction du topic et sa supression de la liste des topics en gerant les acc�s concurents
 * @param topic
 * @throws Exception
 */
	public void destroyTopic(String topic) throws Exception{
		lockMap.get(topic).writeLock().lock();
		topicsSubscriber.remove(topic);
		lockMap.get(topic).writeLock().unlock();
		lockMap.remove(topic);
	}
/**
 * verification de l'existance du topic
 * @param topic
 * @return
 */

	public boolean isTopic(String topic) {
		
		return lockMap.containsKey(topic);
	}
	
/**
 * recuperer tous les topics qui existent et qui sont cr�es par les publisher 
 * @return
 * @throws Exception
 */

	public  String[] getTopics() throws Exception{
		
		Set<String> set=lockMap.keySet(); 
		String[] liste = new String[set.size()];
		int i =0;
		for (String s : set) {
			liste[i++]=s;
		}
		return liste;
	}
	
/**
 * souscrire a un topic publier par un publisher qui a comme uri inboundPortURI
 * @param topic
 * @param inboundPortURI
 * @throws Exception
 */
	public void subscriber(String topic, String inboundPortURI)throws Exception {
	
		subscriberPortsLock.writeLock().lock();
		MapofSubscribers(inboundPortURI);
		
		this.traceMessage(" le subscriber demande de souscrir au topic  : " + topic + " \n");
		this.traceMessage(" le subscriber accepte le message [Ljava.lang.String;@16b4a017\\n1591470281211 \n");
		//verifier l'existance du topic pour pouvoir le creer 
		if(!topicsSubscriber.containsKey(topic)) {
			createTopic(topic);
			
			
		}
		
		
		topicsSubscriber.get(topic).put(inboundPortURI,null);
		subscriberPortsLock.writeLock().unlock();
	}

/**
 * souscrire a plusieurs  topics publier par un publisher qui a comme uri inboundPortURI
 * @param topics
 * @param inboundPortURI
 * @throws Exception
 */
	public void subscriber(String[] topics, String inboundPortURI)throws Exception {
		subscriberPortsLock.writeLock().lock();
		MapofSubscribers(inboundPortURI);
		for ( String s : topics) {
			subscriber(s, inboundPortURI);
		}
		subscriberPortsLock.writeLock().unlock();
		
	}

/**
 * souscrire a un message d'un topic qui a comme filtre (filter) et qui est  publi� par le publisher qui a comme uri inboundPortURI
 * @param topic
 * @param filter
 * @param inboundPortURI
 * @throws Exception
 */
	public void subscriber(String topic, MessageFilterI filter, String inboundPortURI) throws Exception{
		subscriberPortsLock.writeLock().lock();
		MapofSubscribers(inboundPortURI);
		
		subscriber(topic, inboundPortURI);
		topicsSubscriber.get(topic).put(inboundPortURI, filter);
		subscriberPortsLock.writeLock().unlock();
		
	}
/**
 * modifier le filtre d'un message d'un topic par le publisher qui a comme uri inboundPortURI
 * @param topic
 * @param newFilter
 * @param inboundPortURI
 * @throws Exception
 */

	public boolean modifyFilter(String topic, Filter newFilter, String inboundPortURI,MessageI m) throws Exception{
		
		
		boolean filtre=newFilter.filter(m);
		lockMap.get(topic).writeLock().lock();
		if(topicsSubscriber.containsKey(topic) && topicsSubscriber.get(topic).containsKey(inboundPortURI))
			topicsSubscriber.get(topic).put(inboundPortURI, newFilter);
		lockMap.get(topic).writeLock().unlock();
		this.traceMessage("le message "+ m+ "a ete filt� par le filtre qui a comme result"+filtre);
		return filtre;
	}

	
	/**
	 * desinscription du subscriber qui a comme uri inboundPortURI du topic
	 * @param topic
	 * @param inboundPortURI
	 * @throws Exception
	 */
	public void unsubscribe(String topic, String inboundPortURI) throws Exception{
		subscriberPortsLock.writeLock().lock();
		
		this.subscriberPorts.get(inboundPortURI).unpublishPort();
		this.subscriberPorts.get(inboundPortURI).destroyPort();
		this.subscriberPorts.remove(inboundPortURI);
		
		if(this.topicsSubscriber.containsKey(topic)) {
			topicsSubscriber.get(topic).remove(inboundPortURI);
		}
		subscriberPortsLock.writeLock().unlock();
		
	}
/**
 * recuperation du port du composant publisher 
 * @return
 * @throws Exception
 */

	public synchronized String getPublicationURI() throws Exception {
		PublicationInboundPort portPublication = new PublicationInboundPort(this);
		portPublication.publishPort();
		this.traceMessage(" Création de port de publication \n");
		this.listePublisherPort.add(portPublication);
		return portPublication.getPortURI();
	}
	
	
	
}
