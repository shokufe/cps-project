package ComponenentSThreadS;



import java.util.ArrayList;
import java.util.List;

import Message.Filter;
import Message.MessageI;
import connectors.ManagementBrokerSubscriberConnector;
import fr.sorbonne_u.alasca.replication.interfaces.ManagementCI;
import fr.sorbonne_u.alasca.replication.interfaces.ReceptionCI;
import fr.sorbonne_u.alasca.replication.ports.ManagementOutboundPortSubscriber;
import fr.sorbonne_u.alasca.replication.ports.ReceptionInboundPort;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;

/**
 * Composant subscriber 
 * Ce composant demande la souscription a des topics ou des messages publier par le publisher par le biais du broker
 * @author Derbene,sh ahmadi simab
 *
 */

public class Subscriber  extends AbstractComponent{

	protected ReceptionInboundPort portInRec;
	protected ManagementOutboundPortSubscriber portOutManag;
	protected final String[] topics;
	
	protected String URIportManagement;
	
	protected List<MessageI> myMessage  ;
	protected List<String> myMessagee ;
	protected final MessageI[] messages;
	
	protected Subscriber(int uri,String URIportManagement ,int identifiant, String[]topics) throws Exception   {
	
		super(1,0);
		this.topics = topics;
		this.messages = null;
		//this.messages=messages;
		this.addRequiredInterface(ManagementCI.class);
		this.addOfferedInterface(ReceptionCI.class);
		
		this.myMessage= new ArrayList<MessageI>();
		this.myMessagee= new ArrayList<String>();
		myMessagee.add("dac");
		
		this.URIportManagement = URIportManagement;		
		this.portInRec = new ReceptionInboundPort(this);
		this.portOutManag = new ManagementOutboundPortSubscriber(URIportManagement,this);
		
		this.portOutManag.publishPort();
		this.portInRec.publishPort();
		if (AbstractCVM.isDistributed) {
			System.out.println("non local");
			this.executionLog.setDirectory(System.getProperty("user.dir")) ;
		} else {
			//System.out.println(">>>local");
			this.executionLog.setDirectory(System.getProperty("user.home")) ;
			System.out.println(">>> after local");
		}
		
		this.tracer.setTitle("Subscriber: " + identifiant);
		this.tracer.setRelativePosition(identifiant, 1) ;
	
	}
/**
 * methodes de cycles de vies
 */
	@Override
	public void start() throws ComponentStartException {
		super.start();
			this.traceMessage(" Connection au Broker \n") ;
			Filter newFilter=new Filter(topics);
		
			
		

			
			
			for(String t: this.topics) {
			
				this.runTask(new AbstractComponent.AbstractTask() {
					
					@Override
					public void run() {
						try {
							((Subscriber)this.getTaskOwner()).subscribeToTopic(t);
							//((Subscriber)this.getTaskOwner()).acceptMessagee(m);
							

							((Broker)this.getTaskOwner()).modifyFilter(t, newFilter, URIportManagement,null);
						} catch (Exception e) {
							new RuntimeException(e);
						}
					}
		
			});
			
				}
			this.traceMessage("message filtr� par le filtre "+newFilter.toString()+"\n");
			

	
	}
	
	@Override
	public void execute() throws Exception {
		super.execute();
		Filter newFilter=new Filter(topics);
	


		
	}
/**
 * souscription a un topic par le subscriber qui a comme uri inboundPortURI
 * @param topic
 * @param inboundPortURI
 */
	public void subscribeToTopic(String topic, String inboundPortURI) {
		this.traceMessage("je m'abonne au topic : " + topic + "\n");
		this.runTask(
			new AbstractComponent.AbstractTask() {
				@Override
				public void run() {
					try {
						((Subscriber)this.getTaskOwner()).portOutManag.subscribe(topic, inboundPortURI);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});
	}

/**
 * shutdown
 */
	
	@Override
	public void shutdown() throws ComponentShutdownException {
		super.shutdown();
	}
	
/**
 * finalise	
 */
	
	@Override
	public void finalise() throws Exception {
		this.portOutManag.unpublishPort();
		this.portInRec.unpublishPort();
		super.finalise();
	}
	
	@Override
	public void shutdownNow() throws ComponentShutdownException {
		super.shutdownNow();
	}

/**
 * souscrire a un topic (topic)
 * @param topic
 * @throws Exception
 */
	public void subscribeToTopic(String topic) throws Exception {
		this.traceMessage(" Abonnement au topic : " + topic + "\n");
		String port = this.portInRec.getPortURI();
		this.portOutManag.subscribe(topic,port);
	}
	
	
/**
 * accepter un message envoyer par le broker au subscriber. 
 * @param m
 */
	public void acceptMessage(MessageI m) {
		this.traceMessage(" message recu par => " +m.getURI() + "\n");
	
		myMessage.add(m);
	}
	public void acceptMessagee(String m) {
		this.traceMessage(" message recu par => " +m + "\n");
	
		myMessagee.add(m);
	}
/**
 * accepter une liste de messages envoy�es par le broker au subscriber
 * @param ms
 */
	public void acceptMessage(MessageI[] ms){
		for (int i =0 ; i < ms.length ; i++ ) {
			acceptMessage(ms[i]);
		}
	}
	
	
}
