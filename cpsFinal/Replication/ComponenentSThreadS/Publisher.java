package ComponenentSThreadS;

import static org.junit.Assert.assertNotNull;

import Message.Filter;
import Message.Message;
import Message.MessageI;
import Message.Properties;
import Message.TimeStamp;
import connectors.ManagementBrokerPublisherConnector;
import connectors.PublicationConnector;

import fr.sorbonne_u.alasca.replication.interfaces.ManagementCI;
import fr.sorbonne_u.alasca.replication.interfaces.PublicationCI;
import fr.sorbonne_u.alasca.replication.ports.ManagementOuntboundPortPublisher;
import fr.sorbonne_u.alasca.replication.ports.PublicationOutboundPort;

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
/**
 * Le composant publisher ,il se charge de publier des messages et des topics sur le broker 
 * afin que n'importe quel subscriber puisse se souscrire . 
 * @author Derbene,sh ahmadi simab
 *
 */
@RequiredInterfaces(required = {ManagementCI.class, PublicationCI.class})
public class Publisher extends AbstractComponent{

	
	protected String URIportPublication ;
	protected String URIportManagement ; 
	
	public PublicationOutboundPort portOutPub;
	protected ManagementOuntboundPortPublisher  portInManag ;
	protected final String[] messages;
	protected final String[] topics;

	
/**
 * constructor 
 * Dans ce constructor on gere la connection entre nos composants en local entre les compostants(brokerpublisher-subscriber) 
 * ou en reseau entre les differents brokers des differentes jvm
 * @param uri
 * @param URIportManagement
 * @param topicPublisher
 * @param messages
 * @throws Exception
 */
	protected Publisher(int uri,String URIportManagement,String[] topicPublisher,String[] messages) throws Exception {
		super( 0, 1) ;
		
		this.addRequiredInterface(ManagementCI.class);
		this.addRequiredInterface(PublicationCI.class);
		
		this.URIportManagement = URIportManagement;
		this.topics = topicPublisher;
		this.messages = messages;
		
		this.portOutPub = new PublicationOutboundPort(this);
		this.portInManag  = new ManagementOuntboundPortPublisher(URIportManagement,this);
	
		this.portInManag.publishPort();
		this.portOutPub.publishPort();
		if (AbstractCVM.isDistributed) {
			System.out.println("non local");
			this.executionLog.setDirectory(System.getProperty("user.dir")) ;
		} else {
			//System.out.println(">>>local");
			this.executionLog.setDirectory(System.getProperty("user.home")) ;
			System.out.println(">>> after local");
		}
		this.tracer.setTitle("URI du Publisher : " + uri);
		this.tracer.setRelativePosition(uri, 3) ;
	}
/**
 * Cycle de vie
 */
	
	@Override
	public void start() throws ComponentStartException {	
		super.start();
			this.traceMessage(" Connection to Broker by ManagetInboundPort \n") ;
		
			for(String m: this.messages) {
				for(String t: this.topics) {
				this.traceMessage("le publisher publie le message : " + topics+"\\n" );		
				this.runTask(new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						try {
							((Publisher)this.getTaskOwner()).publish(m, t);
					
							
						} catch (Exception e) {
							new RuntimeException(e);
						}
					}
				});
			}
			}
		
	
	}


	/**
	 * create topic avec nom par le publisher
	 * @param string
	 */
	protected void createTopic(String string) {
		this.traceMessage(" Envoie du Message Cotennant comme properties (module ,stl)");
		this.runTask(
			new AbstractComponent.AbstractTask() {
				@Override
				public void run() {
					try {
						((Publisher)this.getTaskOwner()).portInManag.createTopic("dac ");
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
	}
/**
 * publier un message m dans un topic topic.
 * @param m
 * @param topic
 */
	public void	publish(String m, String topic) {
		this.traceMessage(" Envoie du Message Cotennant comme properties (module ,stl)\n");
		Properties properties = new Properties();
		properties.putProp("nom ", "Stl ");
		


		TimeStamp time = new TimeStamp();
		Long time2=time.getTime();
		String timeStamp=time.getTimeStamp();
		TimeStamp timee=new TimeStamp(time2,timeStamp);
		MessageI e = new Message(URIportPublication,timee, properties, m);
		System.out.println(e.toString());

		this.runTask(
			new AbstractComponent.AbstractTask() {
				@Override
				public void run() {
					
					try {
						((Publisher)this.getTaskOwner()).portOutPub.publish(e, topic);
					

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
	}
	
	
/**
 * routourner tous uri des publishers.
 * @return
 * @throws Exception
 */
	public String getPublicationPortURI() throws Exception {
		this.traceMessage(" Demande de port de publication au Broker \n");
		return this.portInManag.getPublicationPortURI();
	}

}
