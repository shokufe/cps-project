package connectors;

import Message.Filter;
import Message.MessageFilterI;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.components.connectors.AbstractConnector;

/**
 * conncetor qui connecte les brokers des differentes jvms en utisants brokerServerInboundPort et BrokerClientOutBoundPort
 * @author DERBENE,AHMADI SIMAB 
 *
 */
public class BrokerClientServerConnector extends AbstractConnector implements BrokerCI {

	
	@Override
	public void subscribe(String topic, String inboundPortURI) throws Exception{
		((BrokerCI)this.offering).subscribe(topic, inboundPortURI);
		
	}

	@Override
	public void subscribe(String [] topic, String inboundPortURI)throws Exception {
		((BrokerCI)this.offering).subscribe(topic, inboundPortURI);
		
	}

	@Override
	public void subscribe(String topic, MessageFilterI filter, String inboundPortURI)throws Exception {
		((BrokerCI)this.offering).subscribe(topic, inboundPortURI);
		
	}

	@Override
	public void modifyFilter(String topic, Filter newFilter, String inboundPortURI,MessageI m) throws Exception{
		((BrokerCI)this.offering).modifyFilter(topic, newFilter, inboundPortURI,m);
		
	}

	@Override
	public void unsubscribe(String topic, String inboundPortURI) throws Exception{
		((BrokerCI)this.offering).unsubscribe(topic, inboundPortURI);
		
	}

	@Override
	public void createTopic(String topic) throws Exception{
		((BrokerCI)this.offering).createTopic(topic);
	}

	@Override
	public void createTopics(String[] topics) throws Exception{
		((BrokerCI)this.offering).createTopics(topics);
		
	}

	@Override
	public void destroyTopic(String topic)throws Exception {
		((BrokerCI)this.offering).destroyTopic(topic);
		
	}

	@Override
	public boolean isTopic(String topic) throws Exception{
		return ((BrokerCI)this.offering).isTopic(topic);
		
	}

	@Override
	public String [] getTopics() throws Exception{
		return	((BrokerCI)this.offering).getTopics();
		
	}

	@Override
	public String getPublicationPortURI() throws Exception{
	
		return ((BrokerCI)this.offering).getPublicationPortURI();
	}
	

	

	@Override
	public String publish(MessageI m, String topic) throws Exception{
		return ((BrokerCI)this.offering).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI m, String[] topic)throws Exception {
		((BrokerCI)this.offering).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI[] m, String[] topic) throws Exception{
		((BrokerCI)this.offering).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI[] m, String topic) throws Exception{
		((BrokerCI)this.offering).publish(m, topic);
		
	}
	@Override
	public void acceptMessage(MessageI m) throws Exception {
			((BrokerCI)this.offering).acceptMessage(m);
		
	}

	@Override
	public void acceptMessages(MessageI[] ms) throws Exception{
		((BrokerCI)this.offering).acceptMessages(ms);
		
	}


}
