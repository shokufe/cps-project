package connectors;

import java.util.ArrayList;

import Message.Filter;
import Message.MessageFilterI;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.ManagementCI;
import fr.sorbonne_u.components.connectors.AbstractConnector;


/**
 *  connctor entre le broker et le subscriber en utilisant le managementinboundport et le managementoutboundport
 * et il permet au subscriber de souscrire au differents topics creer par les  publishers dans le broker
 * @author Derbene,Ahmadi Simab
 *
 */
public class ManagementBrokerSubscriberConnector extends AbstractConnector implements ManagementCI {

	@Override
	public void subscribe(String topic, String inboundPortURI) throws Exception{
		((ManagementCI)this.offering).subscribe(topic, inboundPortURI);

		
	}

	@Override
	public void subscribe(String [] topic, String inboundPortURI) throws Exception{
		((ManagementCI)this.offering).subscribe(topic, inboundPortURI);

		
	}

	@Override
	public void subscribe(String topic, MessageFilterI filter, String inboundPortURI)throws Exception {
		((ManagementCI)this.offering).subscribe(topic, filter, inboundPortURI);

		
	}

	@Override
	public void modifyFilter(String topic, Filter newFilter, String inboundPortURI,MessageI m) throws Exception{
		((ManagementCI)this.offering).modifyFilter(topic, newFilter, inboundPortURI,m);

		
	}

	@Override
	public void unsubscribe(String topic, String inboundPortURI) throws Exception{
		((ManagementCI)this.offering).unsubscribe(topic, inboundPortURI);

		
	}

	@Override
	public void createTopic(String topic) throws Exception{
		((ManagementCI)this.offering).createTopic(topic);

		
	}

	@Override
	public void createTopics(String[] topics) throws Exception{
		((ManagementCI)this.offering).createTopics(topics);

		
	}

	@Override
	public void destroyTopic(String topic)throws Exception {
		((ManagementCI)this.offering).destroyTopic(topic);

		
	}

	@Override
	public boolean isTopic(String topic)throws Exception {
		// TODO Auto-generated method stub
		return ((ManagementCI)this.offering).isTopic(topic);

	}

	@Override
	public String [] getTopics() throws Exception{
		// TODO Auto-generated method stub
		return 	((ManagementCI)this.offering).getTopics();

	}

	@Override
	public String getPublicationPortURI() throws Exception{
		// TODO Auto-generated method stub
		return ((ManagementCI)this.offering).getPublicationPortURI();
	}

}
