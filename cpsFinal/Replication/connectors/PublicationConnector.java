package connectors;

import java.util.ArrayList;

import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.PublicationCI;
import fr.sorbonne_u.components.connectors.AbstractConnector;

/**
 * connector entre le broker et le publisher par les ports publicationoutboundport et publicationinboundport
 * il permet au publisher de publier des topics et des messages dans le broker .
 * @author DERBENE,AHMADI SIMAB
 *
 */
public class PublicationConnector  extends AbstractConnector implements PublicationCI{


	@Override
	public String publish(MessageI m, String topic) throws Exception{
		return ((PublicationCI)this.offering).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI m, String[] topic)throws Exception {
		((PublicationCI)this.offering).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI[] m, String[] topic) throws Exception{
		((PublicationCI)this.offering).publish(m, topic);
		
	}

	@Override
	public void publish(MessageI[] m, String topic) throws Exception{
		((PublicationCI)this.offering).publish(m, topic);
		
	}



}
