package connectors;

import java.util.ArrayList;

import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.ReceptionCI;
import fr.sorbonne_u.components.connectors.AbstractConnector;

/**
 * 
 * @author DERBENE,AHMADI SIMAB
 *Ce connector connecte le broker au subscriber par les ports receptioninboundport et receptionoutboundport
 *ce connector permet au broker d envoyer des messages aux quels le subscriber a souscrit avec l'acceptation de ce dernier
 */
public class ReceptionSubscriberBrokerConnector extends AbstractConnector implements ReceptionCI  {

	@Override
	public void acceptMessage(MessageI m) throws Exception {
			((ReceptionCI)this.offering).acceptMessage(m);
		
	}

	@Override
	public void acceptMessages(MessageI[] ms) throws Exception{
		((ReceptionCI)this.offering).acceptMessages(ms);
		
	}

}
