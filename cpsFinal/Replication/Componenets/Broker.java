package Componenets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import Connector.ManagementBrokerPublisherConnector;
import Connector.ManagementBrokerSubscriberConnector;

import Message.MessageFilterI;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.ManagementCI;
import fr.sorbonne_u.alasca.replication.interfaces.ManagementImplementation;
import fr.sorbonne_u.alasca.replication.interfaces.PublicationCI;
import fr.sorbonne_u.alasca.replication.interfaces.PublicationImplementationI;
import fr.sorbonne_u.alasca.replication.interfaces.ReceptionCI;
import fr.sorbonne_u.alasca.replication.interfaces.TerminationNotificationCI;
import fr.sorbonne_u.alasca.replication.ports.ManagementInboundPort;
import fr.sorbonne_u.alasca.replication.ports.PublicationInboundPort;
import fr.sorbonne_u.alasca.replication.ports.ReceptionOutboundPort;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;

 
/**
 * cette classe est la version dynamique de notre borker
 * @author Derbene,sh ahmadi simab
 *
 */

@OfferedInterfaces(offered = {PublicationCI.class,ManagementCI.class})
@RequiredInterfaces(required = {ReceptionCI.class,TerminationNotificationCI.class})


public class Broker extends AbstractComponent implements ManagementImplementation,PublicationImplementationI{

	protected Function<Integer,Integer>							f ;

	protected ManagementInboundPort			imangport ;
	protected ReceptionOutboundPort			orecport ;
	protected PublicationInboundPort			ipubport ;
     String publisherURII;
     String subscriberURI;
     String brokerURI;

	
	  
 Map<String, Map<String,MessageFilterI >> topicSubscriber = new HashMap<String, Map<String,MessageFilterI >>();
 Map<String,Map<String,String>> messagesTopic = new HashMap<String,Map<String,String>>();
	
 HashMap<String, String> topicMessage = new HashMap<String, String>();
 
 String[] topics ;

/**
 * Constructeur
 * @param brokerURI
 * @param publisherURI
 * @param Subscriber
 * @throws Exception
 */
 protected Broker(String brokerURI,String publisherURI,String Subscriber)  throws Exception{
      

		super(1, 0) ;
		
		this.publisherURII=publisherURI;
	
		this.imangport = new ManagementInboundPort( publisherURI,this);
		
       this.imangport.publishPort();
       
       this.ipubport = new PublicationInboundPort( this);
		
       this.ipubport.publishPort();
       
       
       this.orecport = new ReceptionOutboundPort( this);
		
       this.orecport.publishPort();
   	this.tracer.setTitle("******Broker " +brokerURI) ;
   	this.tracer.setRelativePosition(1, 2) ;

     
   	this.toggleTracing();
      
   
     

	
	}

	protected	Broker(String myURI)
	throws Exception
	{
		this(myURI, null,null) ;
	}


	@Override
	public void	start() throws ComponentStartException
	{
	

		super.start() ;
		
		try {
			
			this.doPortConnection(
					this.imangport.getPortURI(),
					this.publisherURII,
			ManagementBrokerPublisherConnector.class.getCanonicalName()) ;
			this.logMessage(this.imangport.getPortURI()) ;
						System.out.println(publisherURII);
			this.logMessage("portt"+this.imangport.getPortURI()) ;
				this.doPortConnection(
								this.imangport.getPortURI(),
								this.subscriberURI,
								ManagementBrokerSubscriberConnector.class.getCanonicalName()) ;
		
		System.out.println();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
		
		
	}
	
	

	
@Override
public void			finalise() throws Exception {
	if (this.imangport != null) {
	//this.doPortDisconnection(this.imangport.getPortURI()) ;
	}
	super.finalise() ;
}


@Override
public void			shutdown() throws ComponentShutdownException {
	try {
		this.imangport.unpublishPort() ;
		this.ipubport.unpublishPort() ;

		if (this.orecport != null) {
			this.orecport.unpublishPort() ;
		}
	} catch (Exception e) {
		throw new ComponentShutdownException(e) ;
	}
	super.shutdown();
}


/**
 * Create Topic
 */
@Override
	public void createTopic(String topic) throws Exception{
	
//System.out.println("create topic");
 	
 	 for(int i=0;i<topics.length;i++) {
 		 topics[i]=topic;
 	 this.logMessage("topic ajouté" + topics[i]);
 	 }
 
 	
 
		
	}
/**
 * isTopic
 */

	public boolean isTopic(String topic) throws Exception
	
	{
	
   boolean f=false;
   String[] topics=this.getTopics();
 

for(int i=0; i< topics.length;i++) {
	 
	 if(topics[i].equals(topic)){
		
		 f=true;
	 }
	 
}
  this.logMessage("is topic"+ f);
	return f;
}	
	
	

public String[] getTopics() throws Exception
	
	{

	this.logMessage("topics "+""+topics);
	return topics;

	}

public String getPublicationPortURI() throws Exception

{
	return this.ipubport.getPortURI();
}

	//methode du Broker createTopic

	public void createTopics(ArrayList<String> topicList) throws Exception
	
	{
		

for(int i=0;i<topicList.size();i++) {
 if(this.isTopic(topicList.get(i))) {
 	this.logMessage("topic existant");
 }else {
	 this.logMessage("topics..list");
	 
	 for(int j=0;j<topicList.size();j++)
 	 topics[j]=topicList.get(i);

 	 this.logMessage("topic ajouté" + topicList.get(i));
 	 
 }
 }
		
	}
	
	
	


	
 

	
	//methode du publish

	public String publish(MessageI m,String topic) throws Exception
		
	 	{
		this.logMessage("message"+m);
		
			  String uriMessage=m.getURI();
				
			 if(this.isTopic(topic)) {
				 this.logMessage("message **"+ m +"publié");
				 topicMessage.put(topic, uriMessage);
				
				 this.logMessage("message **"+ m +"publié");
				 
				 
		 }else {
			 this.createTopic(topic);
			 
			 this.logMessage("le topic "+ topic +" est crée");
		 }
//			  
			return topic;

				   
				   
		}
	
	/***************a deplacer********************/

	
	//methode du publish plusieur topics pour un meme message

public void publish(MessageI m,ArrayList<String >topic) throws Exception
		
{
	Map <String,String>mesage = new HashMap <String,String>();
	String uriMessage=m.getURI();
			  
	if(messagesTopic.size()!=0) {
			 for (int i=0;i<topic.size();i++) {
				 if(this.isTopic(topic.get(i)))
					{
					 mesage=messagesTopic.get(topic.get(i));
					 mesage.put(m.getURI(), m.getURI());
					 
					 this.logMessage("topic existant ");
					}else {
						this.createTopic(topic.get(i));
						mesage.put(m.getURI(), m.getURI());
						
						
						this.logMessage("le topic "+ topic +" est cree");
					}
				 messagesTopic.put(topic.get(i),mesage);
				 
			 }
	}
	
	}

public void publish(ArrayList<MessageI> m, ArrayList<String>topic) throws Exception{
	
	Map <String,String>mesage = new HashMap <String,String>();
	for(int i=0;i<topic.size();i++){
		if(this.isTopic(topic.get(i)))
		{
			 mesage=messagesTopic.get(topic.get(i));
			for (int j=0;j<m.size();i++) {
			
			 mesage.put(m.get(i).getURI(), m.get(i).getURI());
			}
		}else{
			this.createTopic(topic.get(i));
			for (int j=0;j<m.size();i++) {
				
				 mesage.put(m.get(i).getURI(), m.get(i).getURI());
				}
			this.logMessage("le topic "+ topic.get(i)+" est cree");
		}
		messagesTopic.put(topic.get(i),mesage);
		
	}

	
}

//methode du publish plusieurs messages pour un topic

public void publish(ArrayList<MessageI>m,String topic) throws Exception
	
{
 String uriMessage=null;
	 
	Map <String,String>mesage = new HashMap <String,String>();
	if(messagesTopic.size()!=0) {
		
	for (int i=0;i<m.size();i++) {
		 if(this.isTopic(topic))
			{
			 mesage=messagesTopic.get(topic);
			 mesage.put(m.get(i).getURI(), m.get(i).getURI());
			 
			 this.logMessage("topic existant ");
			}else {
				this.createTopic(topic);
				 mesage.put(m.get(i).getURI(), m.get(i).getURI());
				
				
				this.logMessage("le topic "+ topic +" est cree");
			}
		uriMessage= m.get(i).getURI();
		
	}
	 messagesTopic.put(topic,mesage);
	
	}

		   
			   
	}
		

/**
 * 
 * @param topic
 * @param inboundPortURI
 * @return
 * @throws Exception
 */

	public String subscribe(String topic,String inboundPortURI) throws Exception
	
	{
		Map <String,MessageFilterI >topics = new HashMap <String,MessageFilterI >();

	
	  if(topicSubscriber.size()!=0) {
		  if(this.isTopic(topic)) {
		   topics=  topicSubscriber.get(inboundPortURI);
		   topics.put(topic, null);
	
			  this.logMessage("subscription reussi ");
			  
		  }else {

			  this.createTopic(topic);
			   topics.put(topic, null);

			  this.logMessage("subscription reussi ");
			  
		  }
		 
		  
	  }
	  topicSubscriber.put(inboundPortURI, topics);

	  return topic;
	}

	
	/**
	 * 
	 * @param topic
	 * @param inboundPortURI
	 * @return
	 * @throws Exception
	 */
	
	public String subscribe(ArrayList<String> topic, String inboundPortURI) throws Exception{
		

		Map <String,MessageFilterI >topics = new HashMap <String,MessageFilterI >();

	
	  if(topicSubscriber.size()!=0) {
		  for(int i=0;i<topic.size();i++) {
		  if(this.isTopic(topic.get(i))) {
		   topics=  topicSubscriber.get(inboundPortURI);
		   topics.put(topic.get(i), null);
	
			  this.logMessage("subscription reussi ");
			  
		  }else {

			  this.createTopic(topic.get(i));
			   topics.put(topic.get(i), null);

			  this.logMessage("subscription reussi ");
			  
		  }
		 
		  
	  }
	  topicSubscriber.put(inboundPortURI, topics);

	}

		return inboundPortURI;
	}
/**
 * 		
 * @param topic
 * @param filter
 * @param inboundPortURI
 * @return
 * @throws Exception
 */
	public String subscribe(String topic, MessageFilterI filter, String inboundPortURI) throws Exception{

		Map <String,MessageFilterI >topics = new HashMap <String,MessageFilterI >();

	
	  if(topicSubscriber.size()!=0) {
		  
		  if(this.isTopic(topic)) {
		   topics=  topicSubscriber.get(inboundPortURI);
		   topics.put(topic, filter);
	
			  this.logMessage("subscription reussi ");
			  
		  }else {

			  this.createTopic(topic);
			   topics.put(topic, filter);

			  this.logMessage("subscription reussi ");
			  
		  }
		 
		
	  topicSubscriber.put(inboundPortURI, topics);

	}

		return inboundPortURI;
		
	}
	/**
	 * 	
	 * @param topic
	 * @param newFilter
	 * @param inboundPortURI
	 * @return
	 * @throws Exception
	 */
	
public String modifyFilter(String topic, MessageFilterI newFilter, String inboundPortURI) throws Exception{
		
	Map <String,MessageFilterI >topics = new HashMap <String,MessageFilterI >();

	
	  if(topicSubscriber.size()!=0) {
		  if(this.isTopic(topic)) {
		   topics=  topicSubscriber.get(inboundPortURI);
		   //MessageFilterI filter	=topics.get(topic);
		   topics.put(topic, newFilter);
		   
	
			  this.logMessage("subscription reussi ");
			  
		  }else {

			  this.createTopic(topic);
			   topics.put(topic, newFilter);

			  this.logMessage("subscription reussi ");
			  
		  }
		 
		  
	  }
	  topicSubscriber.put(inboundPortURI, topics);

	  return topic;


	}
		
		
	/**
	 * 	
	 * @param topic
	 * @param inboundPortURI
	 */
		


public void unsubscribe(String topic,String inboundPortURI){
	Map<String, MessageFilterI> topicodSub=  topicSubscriber.get(inboundPortURI);
	if(!topicodSub.get(topic).equals(null)){
		topicodSub.remove(topic);
		topicSubscriber.put(inboundPortURI,topicodSub);
		this.logMessage("vous n'etes plus souscrir pour le topic" + topic);
	}else{
		this.logMessage("vous n'etes pas indcris pour ce topic ");
	}
}	
		
		
/**
 * 		
 */
		

public void destroyTopic(String topic) throws Exception

{



if(this.isTopic(topic)) {
	for(int i=0;i<topics.length;i++) {
     if(topic.equals(topics[i])) {
		topics[i]=topics[i-1];
     }
}}else {

	 this.logMessage("le topic n'existe pas ");

}
}

@Override
public void publish(MessageI m, String[] topic) throws Exception {
	// TODO Auto-generated method stub
	
}

@Override
public void publish(MessageI[] m, String[] topic) throws Exception {
	// TODO Auto-generated method stub
	
}

@Override
public void publish(MessageI[] m, String topic) throws Exception {
	// TODO Auto-generated method stub
	
}

@Override
public void createTopics(String[] topics) throws Exception {
	// TODO Auto-generated method stub
	
}


}
