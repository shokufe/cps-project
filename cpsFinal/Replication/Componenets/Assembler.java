package Componenets;

import java.util.HashSet;
import java.util.Set;


import deployementThread.CVMThread;
import deployemetSimple.CVM;
import fr.sorbonne_u.alasca.replication.interfaces.TerminationNotificationCI;
import fr.sorbonne_u.alasca.replication.ports.TerminationNotificationOutboundPort;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import fr.sorbonne_u.components.pre.dcc.connectors.DynamicComponentCreationConnector;
import fr.sorbonne_u.components.pre.dcc.interfaces.DynamicComponentCreationI;
import fr.sorbonne_u.components.pre.dcc.ports.DynamicComponentCreationOutboundPort;


@RequiredInterfaces(required = {DynamicComponentCreationI.class})
@OfferedInterfaces(offered = {TerminationNotificationCI.class})
/**
 * 
 * @author Derbene,sh ahmadi simab
 *
 */
public class Assembler extends AbstractComponent
{

	
	protected DynamicComponentCreationOutboundPort	dccOutPort ;
//	protected DynamicComponentCreationOutboundPort	dccOutPort1 ;

	protected TerminationNotificationOutboundPort	termNotPort ;
	protected String								jvmURI ;
	protected Set<String>							deployerURIs ;
	
	
	
	protected	Assembler(String jvmURI) throws Exception
	{
		super(1, 0) ;
		this.jvmURI = jvmURI ;

		this.termNotPort = new TerminationNotificationOutboundPort(this) ;
		this.termNotPort.publishPort() ;

		this.tracer.setTitle("Assembler") ;
		this.tracer.setRelativePosition(0, 0) ;
		this.toggleTracing() ;
		

	}

	
	@Override
	public void	start() throws ComponentStartException
	{
		this.logMessage("start assembleur");
		super.start() ;
		try {
			this.dccOutPort = new DynamicComponentCreationOutboundPort(this) ;
			this.dccOutPort.publishPort() ;
	
		
			this.doPortConnection(
				this.dccOutPort.getPortURI(),
				this.jvmURI + AbstractCVM.DCC_INBOUNDPORT_URI_SUFFIX,
				DynamicComponentCreationConnector.class.getCanonicalName()) ;
			
		} catch (Exception e) {
			throw new ComponentStartException(e) ;
		}
	}
	
	@Override
	public void			execute() throws Exception
	{
		super.execute() ;

		this.deployerURIs = new HashSet<String>() ;
	
		
	/*String brokerURI =
				this.dccOutPort.createComponent(
						Broker.class.getCanonicalName(),
						new Object[] {deployemet.CVM.Broker_IBP_URI, deployemet.CVM.Publisher_IBP_URI}) ;
		
		( this.dccOutPort).executeComponent(brokerURI) ;*/		 
		String publisherURI =
				this.dccOutPort.createComponent(
						Publisher.class.getCanonicalName(),
						new Object[] {CVMThread.URI__PUBLISHER,CVMThread.URI__BROKER}) ;
		( this.dccOutPort).executeComponent(publisherURI) ;
		System.out.println(publisherURI);
		
		
		String subscriberURI =
				this.dccOutPort.createComponent(
						Subscriber.class.getCanonicalName(),
						new Object[] {CVMThread.URI__SUBSCRIBER,CVMThread.URI__BROKER}) ;
		( this.dccOutPort).executeComponent(subscriberURI) ;
		System.out.println(publisherURI);
		

	


		
		this.logMessage("Execution...") ;
		
		
		

	} 

/**
 * 
 */


	@Override
	public void			finalise() throws Exception
	{
		this.doPortDisconnection(this.dccOutPort.getPortURI()) ;
		super.finalise();
	}
/**
 * 
 */
	@Override
	public void	shutdown() throws ComponentShutdownException
	{
		try {
			this.dccOutPort.unpublishPort() ;
			this.termNotPort.unpublishPort() ;
		} catch (Exception e) {
			throw new ComponentShutdownException(e) ;
		}
		super.shutdown();
	}

}
