package Componenets;


import java.util.ArrayList;


import Message.Message;
import Message.MessageI;
import Message.Properties;
import fr.sorbonne_u.alasca.replication.interfaces.ManagementCI;
import fr.sorbonne_u.alasca.replication.interfaces.PublicationCI;
import fr.sorbonne_u.alasca.replication.ports.ManagementInboundPort;
import fr.sorbonne_u.alasca.replication.ports.PublicationInboundPort;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;



/**
 * Cette classe est la version dynamique de notre publisher.
 * @author Derbene,sh ahmadi simab
 *
 */

@RequiredInterfaces(required = {PublicationCI.class,ManagementCI.class })
public class Publisher  extends AbstractComponent{

	
	String brokerURI;

	 String[] topics;
	ArrayList<MessageI> messages=new ArrayList<>();
	private String termNotURI;
	
	protected ManagementInboundPort			omangport ;
	protected PublicationInboundPort			opubport ;
	Broker broker;

         //constructeur publisher 
	
	protected Publisher(String publisherURI,String brokerURI)  throws Exception{

		super(1,0) ;
	

	  	this.tracer.setTitle("******publisher " +" "+publisherURI) ;
		this.tracer.setRelativePosition(3, 3) ;
		this.toggleTracing();

				
  	// broker=new Broker(brokerURI);
		
 
	}

	
	//methode execute 
	
	@Override
	public void	execute() throws Exception
	{
	this.logMessage("start execution "+" "+"/n");
	this.logMessage("starting operation");
//		
	Properties p=new Properties();
	Message m =null;
	p.putProp("uri", true);
//	
//	
	

	String topic="algavvvv";
	for(int i=0;i<2;i++) {
		topics[i]=topic;
		topics[i]="stl";
	}
	
	messages.add(m);
//	
//			this.traceMessage(" Publisher the first exchange.\n") ;
	//this.broker.imangport.isTopic(topic);
	broker=new Broker(brokerURI);
			this.broker.imangport.createTopic("stl");
		this.broker.imangport.getTopics();
			this.broker.ipubport.publish(m, topic);
			this.broker.imangport.createTopics(topics);
			//
			
			this.broker.ipubport.publish(m, topics);
		//this.broker.ipubport.publish(messages, topics);
			this.broker.ipubport.publish((MessageI) messages, topic);
	
	
////			
			
			
			
			
			
		
		}
	


}

