package test;



import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.*;

import Message.Filter;
import Message.Message;
import Message.Properties;
import Message.TimeStamp;

public class MessageTest {
 @Test
 public void test1(){
	Properties prop=new Properties();
	prop.putProp("Specialit ", "stl");
	prop.putProp("Module "," CPS");
	
	Message m=new Message("m1", new TimeStamp(0, ""), prop, "send_message");
	Map<String, Object> map=new HashMap<String, Object>();
	map.put("Specialit "," stl");
	map.put("Module ","CPS");
	map.put("type ","projet");
	Filter filter=new Filter(map);
	assertEquals(false, filter.filterMap(m));
   }

 @Test
 public void test2(){
	Properties prop=new Properties();
	prop.putProp("nombre_etudiant ", 100);
	prop.putProp("Specialit ","stl");
	
	Message m=new Message("m1", new TimeStamp(0, ""), prop, "send_message2");
	Map<String, Object> map=new HashMap<String, Object>();
	map.put("nombre_etudiant ", 100);
	map.put("Specialit ","stl");

	Filter filter=new Filter(map);
	assertEquals(false, filter.filterMap(m));
   }
 
 @Test
 public void test3(){
	Properties prop=new Properties();
	prop.putProp("nombre_etudiant ", "100");
	prop.putProp("Specialit ","stl");
	
	Message m=new Message("m1", new TimeStamp(0, ""), prop, "send_message2");
	Map<String, Object> map=new HashMap<String, Object>();
	map.put("nombre_etudiant ", 100);
	map.put("Specialit ","stl");

	Filter filter=new Filter(map);
	assertNotEquals(false, filter.filterMap(m));
   }
 
 
 

 

 
}