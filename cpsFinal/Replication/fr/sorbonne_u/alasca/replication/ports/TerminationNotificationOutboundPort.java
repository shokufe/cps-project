package fr.sorbonne_u.alasca.replication.ports;



import fr.sorbonne_u.alasca.replication.interfaces.TerminationNotificationCI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;



public class			TerminationNotificationOutboundPort
extends		AbstractOutboundPort
implements	TerminationNotificationCI
{
	private static final long serialVersionUID = 1L;

	public				TerminationNotificationOutboundPort(ComponentI owner)
	throws Exception
	{
		super(TerminationNotificationCI.class, owner) ;
	}

	public				TerminationNotificationOutboundPort(
		String uri,
		ComponentI owner
		) throws Exception
	{
		super(uri, TerminationNotificationCI.class, owner);
	}


	@Override
	public void			notifyTermination() throws Exception
	{
		((TerminationNotificationCI)this.connector).notifyTermination() ;
	}
}
// -----------------------------------------------------------------------------
