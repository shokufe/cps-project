package fr.sorbonne_u.alasca.replication.ports;


import ComponenentSThreadS.Broker;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.PublicationCI;

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;


public class PublicationInboundPort  extends AbstractInboundPort implements PublicationCI{


	private static final long serialVersionUID = 1L;
	
	public PublicationInboundPort(ComponentI owner) throws Exception {
		super(PublicationCI.class, owner);
		assert owner instanceof Broker;
	}

	public PublicationInboundPort( String uri ,ComponentI owner) throws Exception {
		super(uri,PublicationCI.class, owner);
		assert owner instanceof Broker;
	}

	

	@Override
	public String publish(MessageI m, String topic) throws Exception {
		try {
			this.owner.handleRequestAsync(
				new AbstractComponent.AbstractService<Void>() {
					@Override
					public Void call() throws Exception {
						((Broker)this.getServiceOwner()).publish(m,topic);
						return null;
					}
				}
			);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return this.getOwner().handleRequestSync(
				o -> ((PublicationCI)o)).publish(m, topic) ;
	}

	@Override
	public void publish(MessageI m, String[] topics) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).Publish(m,topics);
							return null;
						}
					}
					);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void publish(MessageI[] ms, String topic) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).Publish(ms, topic);
							return null;
						}
					}
					);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void publish(MessageI[] ms, String[] topics) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).Publish(ms, topics);
							return null;
						}
					}
					);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
