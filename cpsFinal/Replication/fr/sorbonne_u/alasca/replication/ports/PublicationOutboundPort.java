package fr.sorbonne_u.alasca.replication.ports;


import ComponenentSThreadS.Publisher;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.PublicationCI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;


public class PublicationOutboundPort extends AbstractOutboundPort implements PublicationCI {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PublicationOutboundPort(ComponentI owner) throws Exception {
		super(PublicationCI.class, owner);
		assert owner instanceof Publisher;
		
	}

	@Override
	public String publish(MessageI m, String topic) throws Exception{
		return ((PublicationCI)this.connector).publish(m, topic);
	}

	@Override
	public void publish(MessageI m, String[] topics)throws Exception {
		((PublicationCI)this.connector).publish(m, topics);
	}

	@Override
	public void publish(MessageI[] ms, String topic)throws Exception {
		((PublicationCI)this.connector).publish(ms, topic);
		
	}

	@Override
	public void publish(MessageI[] ms, String[] topics)throws Exception {
		((PublicationCI)this.connector).publish(ms, topics);
		
	}

}
