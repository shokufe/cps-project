package fr.sorbonne_u.alasca.replication.ports;

import ComponenentSThreadS.Publisher;
import Message.Filter;
import Message.MessageFilterI;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.ManagementCI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;


public class ManagementOuntboundPortPublisher extends AbstractOutboundPort implements ManagementCI  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ManagementOuntboundPortPublisher(ComponentI owner) throws Exception {
		super(ManagementCI.class, owner);
		assert owner instanceof Publisher;
	}
	public ManagementOuntboundPortPublisher(String uri,ComponentI owner) throws Exception {
		super(uri,ManagementCI.class,owner);
		assert owner instanceof Publisher;
	}

	@Override
	public void createTopic(String topic) throws Exception {
		((ManagementCI)this.connector).createTopic(topic);
	}

	@Override
	public void createTopics(String[] topic) throws Exception {
		((ManagementCI)this.connector).createTopics(topic);
	}

	@Override
	public void destroyTopic(String topic) throws Exception {
		((ManagementCI)this.connector).destroyTopic(topic);
	}

	@Override
	public boolean isTopic(String topic) throws Exception {
		return ((ManagementCI)this.connector).isTopic(topic);
	}

	@Override
	public String[] getTopics() throws Exception {
		((ManagementCI)this.connector).getTopics();
		return null;
	}

	@Override
	public String getPublicationPortURI() throws Exception {
		return ((ManagementCI)this.connector).getPublicationPortURI();
	}

	@Override
	public void subscribe(String topic, String inboundPortURI) throws Exception {
		((ManagementCI)this.connector).subscribe(topic, inboundPortURI);
	}

	@Override
	public void subscribe(String[] topics, String inboundPortURI) throws Exception {
		((ManagementCI)this.connector).subscribe(topics, inboundPortURI);
	}

	@Override
	public void subscribe(String topic, MessageFilterI filter, String inboundPortURI) throws Exception {
		((ManagementCI)this.connector).subscribe(topic,filter, inboundPortURI);		
	}

	@Override
	public void modifyFilter(String topic, Filter newFilter, String inboundPortURI,MessageI m) throws Exception {
		((ManagementCI)this.connector).subscribe(topic,newFilter, inboundPortURI);
	}

	@Override
	public void unsubscribe(String topic, String inboundPortURI) throws Exception {
		((ManagementCI)this.connector).unsubscribe(topic, inboundPortURI);

	}

	
}
