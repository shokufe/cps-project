package fr.sorbonne_u.alasca.replication.ports;



import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.alasca.replication.interfaces.TerminationNotificationCI;
import fr.sorbonne_u.alasca.replication.interfaces.TerminationNotificationI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ports.AbstractInboundPort;



public class	TerminationNotificationInboundPort
extends		AbstractInboundPort
implements	TerminationNotificationCI
{
	private static final long serialVersionUID = 1L;

	public				TerminationNotificationInboundPort(ComponentI owner)
	throws Exception
	{
		super(TerminationNotificationI.class, owner) ;
		assert	owner instanceof TerminationNotificationI ;
	}

	public				TerminationNotificationInboundPort(
		String uri,
		ComponentI owner
		) throws Exception
	{
		super(uri, TerminationNotificationI.class, owner) ;
		assert	owner instanceof TerminationNotificationI ;
	}


	@Override
	public void	notifyTermination() throws Exception
	{
		this.getOwner().handleRequestAsync(
				new AbstractComponent.AbstractService<Void>() {
					@Override
					public Void call() throws Exception {
						((TerminationNotificationI)this.getServiceOwner()).
																terminate() ;
						return null;
					}
				}) ;
	}

}
// -----------------------------------------------------------------------------
