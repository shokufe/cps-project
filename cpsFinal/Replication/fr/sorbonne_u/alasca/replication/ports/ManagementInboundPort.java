package fr.sorbonne_u.alasca.replication.ports;



import ComponenentSThreadS.Broker;
import Message.Filter;
import Message.MessageFilterI;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.ManagementCI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;



/**  
  @author Derbene,sh ahmadi simab
 **/
 
public class ManagementInboundPort extends AbstractInboundPort implements ManagementCI{

	private static final long serialVersionUID = 1L;
	protected final int	executorIndex ;
	
	
	/**  
	  @param executorIndex
	  @param URI
	  @param owner
	  @throws Exception**/
	 
	public ManagementInboundPort(int executorIndex,String URI,ComponentI owner) throws Exception {

		super(URI,ManagementCI.class, owner);
		assert owner instanceof Broker;
		assert	owner.validExecutorServiceIndex(executorIndex);
		
		this.executorIndex = executorIndex ;
	}
	







	public ManagementInboundPort(String URI, ComponentI owner) throws Exception {
		super(URI,ManagementCI.class, owner);
		assert owner instanceof Broker;

		
		this.executorIndex = 0;
	}








	@Override
	public void createTopic(String topic) throws Exception {
		try {
			this.owner.handleRequestAsync(
				new AbstractComponent.AbstractService<Void>() {
					@Override
					public Void call() throws Exception {
						((Broker)this.getServiceOwner()).createTopic(topic);
						return null;
					}
				});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	  
	 
	@Override
	public void createTopics(String[] topic) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).createTopic(topic);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	  
	 
	@Override
	public void destroyTopic(String topic)throws Exception {
	try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).destroyTopic(topic);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	  
	 
	@Override
	public boolean isTopic(String topic) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Boolean>() {
						@Override
						public Boolean call() throws Exception {
							return ((Broker)this.getServiceOwner()).isTopic(topic);
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String[] getTopics()throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<String[]>() {
						@Override
						public String[] call() throws Exception {
							return ((Broker)this.getServiceOwner()).getTopics();
							
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception (e.getMessage());
		}
		throw new Exception ("No Topics");
		
	}
	
/*	------------------------------------------------------------------------
     ------------------------ Souscriptions -------------------------------  
	------------------------------------------------------------------------*/
	@Override
	public void subscribe(String topic, String inboundPortURI)throws Exception {
		try {
			this.owner.handleRequestAsync(
					executorIndex,
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).subscriber(topic,inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void subscribe(String[] topics, String inboundPortURI) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).subscriber(topics,inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void subscribe(String topic, MessageFilterI filter, String inboundPortURI)throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).subscriber(topic,filter,inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void modifyFilter(String topic, Filter newFilter, String inboundPortURI,MessageI m)throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).modifyFilter(topic,newFilter,inboundPortURI,m);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void unsubscribe(String topic, String inboundPortURI) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).unsubscribe(topic, inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
	  
	 
	@Override
	public String getPublicationPortURI() throws Exception {
		return this.getOwner().handleRequestSync(
			new AbstractComponent.AbstractService<String>() {
				@Override
				public String call() throws Exception {
					return ((Broker)this.getServiceOwner()).getPublicationURI();
				}
		});
	
	}
}
