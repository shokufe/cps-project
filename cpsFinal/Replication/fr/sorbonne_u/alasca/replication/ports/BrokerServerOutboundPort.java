package fr.sorbonne_u.alasca.replication.ports;

import ComponenentSThreadS.Publisher;
import Message.Filter;
import Message.MessageFilterI;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;

public class BrokerServerOutboundPort extends AbstractOutboundPort implements BrokerCI  {

	
	private static final long serialVersionUID = 1L;

	public BrokerServerOutboundPort(ComponentI owner) throws Exception {
		super(BrokerCI.class, owner);
		assert owner instanceof Publisher;
	}
	public BrokerServerOutboundPort(String uri,ComponentI owner) throws Exception {
		super(uri,BrokerCI.class,owner);
		assert owner instanceof Publisher;
	}

	@Override
	public void createTopic(String topic) throws Exception {
		((BrokerCI)this.connector).createTopic(topic);
	}

	@Override
	public void createTopics(String[] topic) throws Exception {
		((BrokerCI)this.connector).createTopics(topic);
	}

	@Override
	public void destroyTopic(String topic) throws Exception {
		((BrokerCI)this.connector).destroyTopic(topic);
	}

	@Override
	public boolean isTopic(String topic) throws Exception {
		return ((BrokerCI)this.connector).isTopic(topic);
	}

	@Override
	public String[] getTopics() throws Exception {
		((BrokerCI)this.connector).getTopics();
		return null;
	}

	@Override
	public String getPublicationPortURI() throws Exception {
		return ((BrokerCI)this.connector).getPublicationPortURI();
	}

	@Override
	public void subscribe(String topic, String inboundPortURI) throws Exception {
		((BrokerCI)this.connector).subscribe(topic, inboundPortURI);
	}

	@Override
	public void subscribe(String[] topics, String inboundPortURI) throws Exception {
		((BrokerCI)this.connector).subscribe(topics, inboundPortURI);
	}

	@Override
	public void subscribe(String topic, MessageFilterI filter, String inboundPortURI) throws Exception {
		((BrokerCI)this.connector).subscribe(topic,filter, inboundPortURI);		
	}

	@Override
	public void modifyFilter(String topic, Filter newFilter, String inboundPortURI,MessageI m) throws Exception {
		((BrokerCI)this.connector).subscribe(topic,newFilter, inboundPortURI);
	}

	@Override
	public void unsubscribe(String topic, String inboundPortURI) throws Exception {
		((BrokerCI)this.connector).unsubscribe(topic, inboundPortURI);

	}
	@Override
	public String publish(MessageI m, String topic) throws Exception{
		return ((BrokerCI)this.connector).publish(m, topic);
	}

	@Override
	public void publish(MessageI m, String[] topics)throws Exception {
		((BrokerCI)this.connector).publish(m, topics);
	}

	@Override
	public void publish(MessageI[] ms, String topic)throws Exception {
		((BrokerCI)this.connector).publish(ms, topic);
		
	}

	@Override
	public void publish(MessageI[] ms, String[] topics)throws Exception {
		((BrokerCI)this.connector).publish(ms, topics);
		
	}

	@Override
	public void acceptMessage(MessageI m) throws Exception {
		((BrokerCI)this.connector).acceptMessage(m);
	}

	@Override
	public void acceptMessages(MessageI[] ms) throws Exception{
		((BrokerCI)this.connector).acceptMessages(ms);
		
	}

}
