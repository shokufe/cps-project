package fr.sorbonne_u.alasca.replication.ports;

import ComponenentSThreadS.Broker;
import ComponenentSThreadS.Subscriber;
import Message.Filter;
import Message.MessageFilterI;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.BrokerCI;
import fr.sorbonne_u.alasca.replication.interfaces.ManagementCI;
import fr.sorbonne_u.alasca.replication.interfaces.PublicationCI;
import fr.sorbonne_u.alasca.replication.interfaces.ReceptionCI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;

public class BrokerClientInboundPort extends AbstractInboundPort implements BrokerCI{
	private static final long serialVersionUID = 1L;
	protected final int	executorIndex ;
	
	public BrokerClientInboundPort(int executorIndex,String URI,ComponentI owner) throws Exception {
		super(URI,ManagementCI.class, owner);
		assert owner instanceof Broker;
		assert	owner.validExecutorServiceIndex(executorIndex);
		
		this.executorIndex = executorIndex ;
	}

	@Override
	public void createTopics(String[] topic) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).createTopic(topic);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	  
	 
	@Override
	public void destroyTopic(String topic)throws Exception {
	try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).destroyTopic(topic);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	  
	 
	@Override
	public boolean isTopic(String topic) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Boolean>() {
						@Override
						public Boolean call() throws Exception {
							return ((Broker)this.getServiceOwner()).isTopic(topic);
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String[] getTopics()throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<String[]>() {
						@Override
						public String[] call() throws Exception {
							return ((Broker)this.getServiceOwner()).getTopics();
							
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception (e.getMessage());
		}
		throw new Exception ("No Topics");
		
	}
	
/*	------------------------------------------------------------------------
     ------------------------ Souscriptions -------------------------------  
	------------------------------------------------------------------------*/
	@Override
	public void subscribe(String topic, String inboundPortURI)throws Exception {
		try {
			this.owner.handleRequestAsync(
					executorIndex,
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).subscriber(topic,inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void subscribe(String[] topics, String inboundPortURI) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).subscriber(topics,inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void subscribe(String topic, MessageFilterI filter, String inboundPortURI)throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).subscriber(topic,filter,inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void modifyFilter(String topic, Filter newFilter, String inboundPortURI,MessageI m)throws Exception {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).modifyFilter(topic,newFilter,inboundPortURI,m);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void unsubscribe(String topic, String inboundPortURI) throws Exception{
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).unsubscribe(topic, inboundPortURI);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
	  
	 
	@Override
	public String getPublicationPortURI() throws Exception {
		return this.getOwner().handleRequestSync(
			new AbstractComponent.AbstractService<String>() {
				@Override
				public String call() throws Exception {
					return ((Broker)this.getServiceOwner()).getPublicationURI();
				}
		});
	
	}

	@Override
	public String publish(MessageI m, String topic) throws Exception {
		try {
			this.owner.handleRequestAsync(
				new AbstractComponent.AbstractService<Void>() {
					@Override
					public Void call() throws Exception {
						((Broker)this.getServiceOwner()).publish(m,topic);
						return null;
					}
				}
			);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return this.getOwner().handleRequestSync(
				o -> ((PublicationCI)o)).publish(m, topic) ;
	}

	@Override
	public void publish(MessageI m, String[] topics) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).Publish(m,topics);
							return null;
						}
					}
					);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void publish(MessageI[] ms, String topic) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).Publish(ms, topic);
							return null;
						}
					}
					);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void publish(MessageI[] ms, String[] topics) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Broker)this.getServiceOwner()).Publish(ms, topics);
							return null;
						}
					}
					);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	@Override
	public void acceptMessage(MessageI m) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Subscriber)this.getServiceOwner()).acceptMessage(m);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void acceptMessages(MessageI[] ms) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Subscriber)this.getServiceOwner()).acceptMessage(ms);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void createTopic(String topic) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
