package fr.sorbonne_u.alasca.replication.ports;


import ComponenentSThreadS.Broker;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.ReceptionCI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;


public class ReceptionOutboundPort extends AbstractOutboundPort implements ReceptionCI{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReceptionOutboundPort(String URI,ComponentI owner) throws Exception {
		super(URI, ReceptionCI.class, owner);
		assert owner instanceof Broker;
	}
	
	public ReceptionOutboundPort( ComponentI owner ) throws Exception {
		super(ReceptionCI.class, owner);
		assert owner instanceof Broker;
	}

	

	@Override
	public void acceptMessage(MessageI m) throws Exception {
		((ReceptionCI)this.connector).acceptMessage(m);
	}

	@Override
	public void acceptMessages(MessageI[] ms) throws Exception{
		((ReceptionCI)this.connector).acceptMessages(ms);
		
	}
}
