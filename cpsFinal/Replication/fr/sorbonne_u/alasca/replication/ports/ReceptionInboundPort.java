package fr.sorbonne_u.alasca.replication.ports;


import ComponenentSThreadS.Subscriber;
import Message.MessageI;
import fr.sorbonne_u.alasca.replication.interfaces.ReceptionCI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;


public class ReceptionInboundPort  extends AbstractInboundPort implements ReceptionCI{
	
	private static final long serialVersionUID = 1L;
	
	public ReceptionInboundPort(ComponentI owner) throws Exception {
		super(ReceptionCI.class, owner);
		assert owner instanceof Subscriber;
	}
	
	public ReceptionInboundPort(String uri ,ComponentI owner) throws Exception {
		super(uri,ReceptionCI.class, owner);
		assert owner instanceof Subscriber;
	}

	@Override
	public void acceptMessage(MessageI m) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Subscriber)this.getServiceOwner()).acceptMessage(m);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void acceptMessages(MessageI[] ms) {
		try {
			this.owner.handleRequestAsync(
					new AbstractComponent.AbstractService<Void>() {
						@Override
						public Void call() throws Exception {
							((Subscriber)this.getServiceOwner()).acceptMessage(ms);
							return null;
						}
					}
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
