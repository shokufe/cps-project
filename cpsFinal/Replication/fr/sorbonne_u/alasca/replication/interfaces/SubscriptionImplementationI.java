package fr.sorbonne_u.alasca.replication.interfaces;

import java.util.ArrayList;

import Message.Filter;
import Message.MessageFilterI;
import Message.MessageI;

public interface SubscriptionImplementationI {
	
	public void subscribe(String topic,String inboundPortURI) throws Exception;
	
	public void subscribe(String []topic,String inboundPortURI) throws Exception;
	
	public void subscribe(String topic,MessageFilterI filter,String inboundPortURI)throws Exception;
	
	
	public void modifyFilter(String topic,Filter newFilter,String inboundPortURI,MessageI m)throws Exception;
	
	 public void unsubscribe(String topic,String inboundPortURI) throws Exception;

}
