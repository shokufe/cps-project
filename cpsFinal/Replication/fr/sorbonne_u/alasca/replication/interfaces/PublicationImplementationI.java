package fr.sorbonne_u.alasca.replication.interfaces;

import java.util.ArrayList;

import Message.MessageI;

public interface PublicationImplementationI {
	
	public String publish(MessageI m,String topic) throws Exception;
	public void publish(MessageI m,String[]topic) throws Exception;
	public void publish(MessageI[] m,String[]topic) throws Exception;
	public void publish(MessageI[] m,String topic) throws Exception;


	

}
