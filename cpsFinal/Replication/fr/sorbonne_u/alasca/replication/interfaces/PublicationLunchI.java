package fr.sorbonne_u.alasca.replication.interfaces;

import fr.sorbonne_u.components.interfaces.OfferedI;
import fr.sorbonne_u.components.interfaces.RequiredI;

public interface PublicationLunchI extends OfferedI ,RequiredI {
	public void	getURIandPrint() throws Exception ;

}
