package fr.sorbonne_u.alasca.replication.interfaces;


import fr.sorbonne_u.components.interfaces.OfferedI;
import fr.sorbonne_u.components.interfaces.RequiredI;


public interface		TerminationNotificationCI
extends		OfferedI,
			RequiredI
{
	public void			notifyTermination() throws Exception ;
}
// -----------------------------------------------------------------------------
