package fr.sorbonne_u.alasca.replication.interfaces;



import fr.sorbonne_u.components.interfaces.OfferedI;
import fr.sorbonne_u.components.interfaces.RequiredI;

public interface ManagementCI extends OfferedI,RequiredI, SubscriptionImplementationI,ManagementImplementation{
	
	
}