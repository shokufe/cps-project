package fr.sorbonne_u.alasca.replication.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import Message.MessageI;

public interface RemoteInterface extends Remote{
	public void createTopic(String topic ) throws RemoteException;
	public void destroyTopic(String topic )throws RemoteException;
	public boolean isTopic(String topic ) throws RemoteException;

	public String publish(String m,String topic) throws RemoteException;

	public void acceptMessage(MessageI m) throws RemoteException;

	public void subscribe(String topic,String inboundPortURI) throws RemoteException;


}
