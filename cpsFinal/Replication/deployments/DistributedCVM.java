package deployments;




import ComponenentSThreadS.Broker;
import connectors.BrokerClientServerConnector;
import connectors.ManagementBrokerPublisherConnector;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.components.cvm.AbstractDistributedCVM;
import fr.sorbonne_u.components.examples.basic_cs.components.URIConsumer;
import fr.sorbonne_u.components.examples.basic_cs.components.URIProvider;
import fr.sorbonne_u.components.examples.basic_cs.connectors.URIServiceConnector;
import fr.sorbonne_u.components.helpers.CVMDebugModes;
/**
 * 
 * distributed cvm permet de relier les differents composants de differentes jvm
 * cette connection se fait entre les differents brokers qui a leurs tours sont relier au publisher et subscriber de leur jvm
 * tels que le broker demandeur de service et le broker server et cette demande se fait par son port entrant brokerServerinboundport
 * et le broker recepteur et le broker client qui recoit par le biais de son port sortant brokerClientoutboundport.
 * 
 * @author kahina
 *
 */

public class	DistributedCVM
extends		AbstractDistributedCVM
{
	protected static final String	BROKERSERVER1_COMPONENT_URI = "my-URI-brokerserver1" ;
	protected static final String	BROKERSERVER2_COMPONENT_URI = "my-URI-brokerserver2" ;
	protected static final String	BROKERCLIENT1_COMPONENT_URI = "my-URI-brokerclient1" ;

	// URI of the CVM instances as defined in the config.xml file
	protected static String			BROKERSERVER2_JVM_URI = "brokerServer2" ;
	protected static String			brokerServer1 = "brokerServer1" ;
	protected static String			brokerClient1 = "brokerClient1" ;
	protected static String			BROKERCLIENT2_JVM_URI = "brokerClient2" ;
	protected static String			BROKERCLIENT3_JVM_URI = "brokerClient3" ;




	protected static String			URIBrokerServerOutboundPortURI = "managOport" ;
	protected static String			URIBrokerClientInboundPortURI = "managIport" ;

	/** Reference to the broker component to share between deploy
	 *  and shutdown.													*/
	protected String	uriBrokerServer1URI ;
	/** Reference to the publisher component to share between deploy
	 *  and shutdown.													*/
	protected String	uriServer2URI ;
	
	/** Reference to the subscriber component to share between deploy
	 *  and shutdown.													*/
	protected String	uriClient1URI ;

	public	DistributedCVM(String[] args, int xLayout, int yLayout)
	throws Exception
	{
		super(args, xLayout, yLayout);
		System.out.println("constructeur dcvm");

		
	}
	public DistributedCVM(String[] args) throws Exception {
		super(args);
	}


	@Override
	public void			initialise() throws Exception
	{
		
/*	AbstractCVM.DEBUG_MODE.add(CVMDebugModes.PUBLIHSING) ;
	AbstractCVM.DEBUG_MODE.add(CVMDebugModes.CONNECTING) ;
	AbstractCVM.DEBUG_MODE.add(CVMDebugModes.COMPONENT_DEPLOYMENT);*/
	
	System.out.println("fin initialise");
		super.initialise() ;

	}


	@Override
	public void			instantiateAndPublish() throws Exception
	{
		
		System.out.println("instantiateAndPublish");
		if (thisJVMURI.equals(brokerServer1)) {

			// create the provider component
			System.out.println("avant broker");
			this.uriBrokerServer1URI =
					AbstractComponent.createComponent(
							Broker.class.getCanonicalName(),
							new Object[]{BROKERSERVER1_COMPONENT_URI,
										 URIBrokerServerOutboundPortURI}) ;
			System.out.println("inst");
			assert	this.isDeployedComponent(this.uriBrokerServer1URI) ;
		
			// make it trace its operations; comment and uncomment the line to see
			// the difference
			this.toggleTracing(this.uriBrokerServer1URI) ;
			this.toggleLogging(this.uriBrokerServer1URI) ;
		// 	assert	this.uriConsumerURI == null && this.uriProviderURI != null ;
		
		} else if (thisJVMURI.equals(brokerClient1)) {

			// create the consumer component
			this.uriClient1URI =
					AbstractComponent.createComponent(
							Broker.class.getCanonicalName(),
							new Object[]{BROKERCLIENT1_COMPONENT_URI,
										 URIBrokerClientInboundPortURI}) ;
			assert	this.isDeployedComponent(this.uriClient1URI) ;
			// make it trace its operations; comment and uncomment the line to see
			// the difference
			this.toggleTracing(this.uriClient1URI) ;
			this.toggleLogging(this.uriClient1URI) ;
		//	assert	this.uriClient1URI != null && this.uriProviderURI == null ;

		} else {

			System.out.println("Unknown JVM URI... " + thisJVMURI) ;

		}

		super.instantiateAndPublish();
	}


	@Override
	public void			interconnect() throws Exception
	{

		System.out.println("interconnect");
		assert	this.isIntantiatedAndPublished() ;
		
		if (thisJVMURI.equals(brokerServer1)) {

			assert	this.uriClient1URI == null && this.uriBrokerServer1URI != null ;

		} else if (thisJVMURI.equals(brokerClient1)) {

			assert	this.uriClient1URI != null && this.uriBrokerServer1URI == null ;
			// do the connection
			this.doPortConnection(
				this.uriClient1URI,
				URIBrokerServerOutboundPortURI,
				URIBrokerClientInboundPortURI,
				BrokerClientServerConnector.class.getCanonicalName());

			System.out.println("apres interconnect");
		} else {

			System.out.println("Unknown JVM URI... " + thisJVMURI) ;

		}

		super.interconnect();
	}

	@Override
	public void			finalise() throws Exception
	{


		if (thisJVMURI.equals(brokerServer1)) {

			assert	this.uriClient1URI == null && this.uriBrokerServer1URI != null ;
			// nothing to be done on the provider side

		} else if (thisJVMURI.equals(brokerClient1)) {

			assert	this.uriClient1URI != null && this.uriBrokerServer1URI == null ;
			this.doPortDisconnection(this.uriClient1URI, URIBrokerServerOutboundPortURI) ;

		} else {

			System.out.println("Unknown JVM URI... " + thisJVMURI) ;

		}

		super.finalise() ;
	}


	@Override
	public void			shutdown() throws Exception
	{
		if (thisJVMURI.equals(brokerServer1)) {

			assert	this.uriClient1URI == null && this.uriBrokerServer1URI != null ;
			// any disconnection not done yet can be performed here

		} else if (thisJVMURI.equals(brokerClient1)) {

			assert	this.uriClient1URI != null && this.uriBrokerServer1URI == null ;
			// any disconnection not done yet can be performed here

		} else {

			System.out.println("Unknown JVM URI... " + thisJVMURI) ;

		}

		super.shutdown();
	}

/*	public static void	main(String[] args)
	{
		String []jvms = {"brokerServer1","config.xml"};
		try {
			
System.out.println("hello");
			DistributedCVM da  = new DistributedCVM(jvms,2,5) ;
			System.out.println(">>HEllo");
			da.startStandardLifeCycle(15000L) ;
			Thread.sleep(10000L) ;
			System.exit(0) ;
		} catch (Exception e) {
			throw new RuntimeException(e) ;
		}
	}*/
	public static void main(String[] args) {
		try {
		
			DistributedCVM da = new DistributedCVM(args, 2, 5);
		
			da.startStandardLifeCycle(15000L);
			Thread.sleep(10000L);
			System.exit(0);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
//-----------------------------------------------------------------------------
