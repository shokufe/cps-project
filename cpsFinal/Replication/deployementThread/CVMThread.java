package deployementThread;




import ComponenentSThreadS.Broker;
import ComponenentSThreadS.Publisher;
import ComponenentSThreadS.Subscriber;
import Message.Message;
import Message.MessageI;
import Message.Properties;
import Message.TimeStamp;
import connectors.ManagementBrokerPublisherConnector;
import connectors.ManagementBrokerSubscriberConnector;
import connectors.PublicationConnector;
import fr.sorbonne_u.alasca.replication.interfaces.PortFactoryI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.components.ports.InboundPortI;
import fr.sorbonne_u.components.ports.OutboundPortI;



/**
 * 
 * @author Derbene,sh ahmadi simab
 * Cette cvm permet de creer les differents composants et de les connecter en eux
 * pour pouvoir lancer les differentes methodes de publications souscriptions ...etc faites par chacun des publisher et des subscriber
 * cette cvm est lanc� en local c'est a dire entre les composants de la meme jvm
 *
 */
public class CVMThread  extends AbstractCVM {
	public static final String URI__PUBLISHER_OUT= "uriPubOut";
	public static final String URI__PUBLISHER= "uriPub";
	public static final String URI__SUBSCRIBER= "uriSub";
	public static final String URI__BROKER= "uribrok";
	public static final String URI__SUBSCRIBER_OUT= "urisubOut";
	protected String uriPublisher;
	protected String uriBroker;
	protected String uriSubscriber ;
	public static final String URI__BROKER_IN= "uriBrokerin";
	public static int NB_SUBSCRIBER = 3; 
	public static String subscriberTopics[][] = { {"stl", "Cps"}, {"Dac", "SAM"}, {"Dac"}};
	public static String PublishTopics[] = { "STL","DAC"};
	public static String PublishMessages[] = { "pstlProject","DACProject"};
	
/**
 * constructor
 * @throws Exception
 */
	
	public CVMThread() throws Exception {
		super(false);
		
	}
	/**
	 * cette methode dploy permet de lancer et creer nos composants 
	 */
	@Override
	public void deploy() throws Exception {
		
		
	for (int i = 0; i < NB_SUBSCRIBER; i++) {
	
		 uriSubscriber =
				AbstractComponent.createComponent(
						Subscriber.class.getCanonicalName(), 
						new Object[]{i,URI__SUBSCRIBER_OUT,i, subscriberTopics[i]}
				);
		 System.out.println("uriiiiiiiiiiii"+uriSubscriber);

		
			this.toggleTracing(uriSubscriber);
	}
		
	 uriBroker=
				AbstractComponent.createComponent(
						Broker.class.getCanonicalName(), 
						new Object[]{URI__BROKER, URI__BROKER_IN,3,3,3,3}
				);
		this.toggleTracing(uriBroker);
		
		
		for (int i = 0; i < NB_SUBSCRIBER; i++) {
			
		 uriPublisher =
				AbstractComponent.createComponent(
						Publisher.class.getCanonicalName(), 
						new Object[]{i,URI__PUBLISHER_OUT, PublishTopics,PublishMessages}
				);
			this.toggleTracing(uriPublisher);
		}
	//connection broker publisher pour la publication des topics et messages
			System.out.println("avant nnnn");

			this.doPortConnection(
					this.uriPublisher, 
					URI__PUBLISHER_OUT,
					URI__BROKER_IN,
					PublicationConnector.class.getCanonicalName()
				);
			System.out.println("nnnn");
			// cinnection broker publisher pour la creation des differents topics
		this.doPortConnection(
				this.uriPublisher, 
				URI__PUBLISHER_OUT,
				URI__BROKER_IN,
				ManagementBrokerPublisherConnector.class.getCanonicalName()
				);
		System.out.println("cvm apres pub");
		
//connection broker subscriber pour la souscription au differents port		
		this.doPortConnection(
				uriSubscriber, 
				URI__SUBSCRIBER_OUT, 
				URI__BROKER_IN,
			ManagementBrokerSubscriberConnector.class.getCanonicalName()
				);
		super.deploy();
		assert	this.deploymentDone() ;
	}

	/**
	 * main methode
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			CVMThread cvm = new CVMThread() ;
			cvm.startStandardLifeCycle(60000L) ;
			Thread.sleep(5000L) ;
			System.exit(0) ;
		} catch (Exception e) {
			throw new RuntimeException(e) ;
		}	
	}

}

